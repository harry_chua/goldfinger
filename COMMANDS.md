## Vision Command Lines:

```sh
rosrun rc_visard_driver rc_visard_driver _device:=:02940576 _enable_tf:=True _autostart_dynamics:=True _autostop_dynamics:=True
```

```sh
rviz -d $(rospack find rc_visard_driver)/config/rc_visard.rviz
```

```sh
rosrun vision_handler pcl_handling_server
```

```sh
rostopic pub /pcl_handling_server/goal vision_handler/PCLHandlerActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  task: 1
  twoDPose:
  - 0"
```

## UR5e Command Lines:

```sh
roslaunch ur_robot_driver ur5e_bringup.launch robot_ip:=192.168.10.201 kinematics_config:=$(rospack find ur5_pap_description)/ur5_robot_calibration.yaml
```

> Switch on the controller

```sh
roslaunch ur5_moveit_config_virtual ur5e_bridge.launch
```

```sh
rosrun ur5_pap_actionlib robot_motion_control_server. y
```

```sh
rostopic pub /ur5_control_server/goal ur5_pap_actionl/MotionControlActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  task: 7
  goalJoints:
  - 0
  goalPose:
    position:
      x: 0.0
      y: 0.0
      z: 0.0
    orientation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 0.0"
```

## Entire Programme

```sh
roslaunch ur_robot_driver ur5e_bringup.launch robot_ip:=192.168.10.201 kinematics_config:=$(rospack find ur5_pap_description)/ur5_robot_calibration.yaml
```

```sh
roslaunch ur5_moveit_config_virtual ur5e_bridge.launch
```

```sh
rosrun rc_visard_driver rc_visard_driver _device:=:02940576 _enable_tf:=False

rosrun rc_visard_driver rc_visard_driver _device:=:02940576 _enable_tf:=False _camera_exp_width:=391 _camera_exp_height:=249 _camera_exp_offset_x:=355 _camera_exp_offset_y:=384

```

```sh
rosrun vision_handler aruco_detector.py
```

```sh
rosrun vision_handler pcl_handling_server
```

```sh
rosrun ur5_gripper ur5_controller.py
```

```sh
rosrun ur5_pap_actionlib robot_motion_control_server.py
```

```sh
rosrun program_manager ur5_program_manager.py
```

```sh
rosrun smach_viewer smach_viewer.py
```

## For Data Collecting

rosrun image_view image_view image:=/stereo/left/image_rect_color

## For AI Vision Server

```sh
conda activate fyp_ur5
cd bridge5e_ws
source install/setup.bash
rosrun ai_vision_server ai_server.py
```

## Launch

roslaunch ur_robot_driver ur5e_bringup.launch

roslaunch vision_handler vision_handler.launch

## Default launcher for ur5
./ur5_init.sh

## Default launcher for ib2
./ib2_init.sh