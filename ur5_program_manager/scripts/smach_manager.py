#!/usr/bin/env python

import rospy
import smach

# define state GetInstrumentsVision
class GetInstrumentsVision(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['exist', 'non_exist'])
        self.existent = False

    def execute(self, userdata):
        rospy.loginfo('Executing state GETINSTRUMENTSVISION')
        if self.existent:
            return 'exist'
        else:
            return 'non_exist'

# define state PickInspectingScissors
class PickInspectingScissors(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['picked'])

    def execute(self, userdata):
        rospy.loginfo('Executing state PICKINSPECTINGSCISSORS')
        return 'picked'

# define state GetInspectionResult
class GetInspectionResult(smach.State):
    def __init__(self):
        smach.State.__init__(self,
                             outcomes=['continue_inspection',
                                       'replace_scissors'],
                             # input_keys=['scissors_state'],
                             output_keys=['scissors_state'])

    def execute(self, userdata):
        if userdata.scissors_state == True:
            return 'continue_inspection'
        else:
            return 'replace_scissors'

# main
def main():
    rospy.init_node('ur5_program_manager')

    # Create a SMACH state machine
    sm = smach.StateMachine(
        outcomes=['new_inspection', 'done_inspection'])

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('GETINSTRUMENTSVISION',
                               GetInstrumentsVision(),
                               transitions={'exist': 'PICKINSPECTINGSCISSORS', 'non_exist': 'GETINSTRUMENTSVISION'})
        smach.StateMachine.add('PICKINSPECTINGSCISSORS',
                               PickInspectingScissors(),
                               transitions={'picked': 'done_inspection'})

    # Execute SMACH plan
    outcome = sm.execute()


if __name__ == '__main__':
    main()
