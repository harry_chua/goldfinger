#!/usr/bin/env python

import rospy
import actionlib
import ur5_pap_actionlib.msg
import geometry_msgs.msg
import math


def moveToPose_client():
    client = actionlib.SimpleActionClient(
        'ur5_control_server', ur5_pap_actionlib.msg.MotionControlAction)

    client.wait_for_server()

    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.x = -0.706971704812
    pose_goal.orientation.y = 0.706939291727
    pose_goal.orientation.z = -0.0146725686459
    pose_goal.orientation.w = 0.0145795109077
    pose_goal.position.x = 0.541043376032
    pose_goal.position.y = -0.133284113571
    pose_goal.position.z = 0.820088093407

    goal = ur5_pap_actionlib.msg.MotionControlGoal(
        task=ur5_pap_actionlib.msg.MotionControlGoal.MOVE_WITH_POSE,
        goalPose=pose_goal)

    # goal = ur5_pap_actionlib.msg.MotionControlGoal(
    #     task=ur5_pap_actionlib.msg.MotionControlGoal.MOVE_WITH_POSE_OFFSET,
    #     goalPose=pose_goal)

    client.send_goal(goal)

    client.wait_for_result()

    print client.get_result()


if __name__ == '__main__':
    try:
        rospy.init_node('move_to_pose_client')
        result = moveToPose_client()
    except rospy.ROSInterruptException:
        print 'byebye'
