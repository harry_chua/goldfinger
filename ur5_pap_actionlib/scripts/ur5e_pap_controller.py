#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import std_msgs.msg
import math
from moveit_commander.conversions import pose_to_list
from tf.transformations import *


def all_close(goal, actual, tolerance):
    """
    @param: goal A list of floats, a Pose or a PoseStamped
    @param: actual A list of floats, a Pose or a PoseStamped
    @param: tolerance A float
    @return: bool
    """
    all_equal = True
    if type(goal) is list:
        for index in range(len(goal)):
            if(abs(actual[index] - goal[index]) > tolerance):
                return False
    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)
    elif type(goal) is geometry_msgs.msg.Pose:
        return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)
    return True


class UR5eMotionPlanningAndControl:
    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)

        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()

        self.current_group = 'instrument_manipulator'
        self.move_group = moveit_commander.MoveGroupCommander(
            self.current_group)

        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                            moveit_msgs.msg.DisplayTrajectory,
                                                            queue_size=20)

        print("Planning frame: %s" % self.move_group.get_planning_frame())
        print("End effector link: %s" %
              self.move_group.get_end_effector_link())
        print("Available planning groups: ", self.robot.get_group_names())
        print("Printing robot's state")
        print(self.robot.get_current_state())
        print(self.move_group.get_named_targets())
        print("")

    def go_to_joint_state(self, joints):
        joint_goal = self.move_group.get_current_joint_values()
        joint_goal[0] = joints[0]
        joint_goal[1] = joints[1]
        joint_goal[2] = joints[2]
        joint_goal[3] = joints[3]
        joint_goal[4] = joints[4]
        joint_goal[5] = joints[5]
        print(joint_goal)  # For debugging

        self.move_group.go(joints, wait=True)
        self.move_group.stop()

        # For verifying:
        current_joints = self.move_group.get_current_joint_values()
        return all_close(joints, current_joints, 0.01)

    def go_to_pose_goal(self, pose):
        # wpose = geometry_msgs.msg.Pose()

        # wpose.position.x = 0.563367721925
        # wpose.position.y = 0.272866770267
        # wpose.position.z = 0.870522880351

        # wpose.orientation.x = 0.0214195929049
        # wpose.orientation.y = -0.999770281843
        # wpose.orientation.z = 0.000764549314209
        # wpose.orientation.w = 6.86840520362e-06

        self.move_group.set_pose_target(pose)

        # Path planning and executing
        plan = self.move_group.plan()
        self.move_group.go(wait=True)
        # self.move_group.go(wait=True)

        self.move_group.stop()

        # Targets clearing
        self.move_group.clear_pose_targets()

        # For verifying:
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose, current_pose, 0.01)

    # To be used when performing pick and place of instruments
    def go_to_pose_goal_with_cartesian_path(self, pose, y_offset=0, z_offset=0, check_collision=False, gripped=False):
        # Cartesian Paths
        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        if not check_collision:
            wpose.position.y += pose.position.y - \
                wpose.position.y + y_offset  # Move along y-axis
            wpose.position.x += pose.position.x - wpose.position.x  # Move along x-axis
            wpose.orientation.x = pose.orientation.x
            wpose.orientation.y = pose.orientation.y
            wpose.orientation.z = pose.orientation.z
            wpose.orientation.w = pose.orientation.w
            waypoints.append(copy.deepcopy(wpose))
            if(not gripped):
                wpose.position.z += (pose.position.z + z_offset) - \
                    wpose.position.z  # Move along z-axis
            else:
                wpose.position.z += (pose.position.z + 0.2) - \
                    wpose.position.z  # Move along z-axis
            waypoints.append(copy.deepcopy(wpose))
        else:
            # wpose.position.x = wpose.position.x
            # wpose.position.y = wpose.position.y
            wpose.position.z += z_offset
            # wpose.orientation.x = wpose.orientation.x
            # wpose.orientation.y = wpose.orientation.y
            # wpose.orientation.z = wpose.orientation.z
            # wpose.orientation.w = wpose.orientation.w
            waypoints.append(copy.deepcopy(wpose))

        # success_percentage = 0
        # while(success_percentage < 0.9):
        #     (plan, fraction) = self.move_group.compute_cartesian_path(
        #         waypoints,  # waypoints to follow
        #         0.01,       # eff_step
        #         0.0)        # jump_threshold
        #     success_percentage = fraction

        # # Plan filtering and execution
        # plan = self.check_trajectory_plan(plan)
        # if (plan):
        #     self.move_group.execute(plan, wait=True)
        #     self.move_group.stop()

        # # For verifying:
        # # current_pose = self.move_group.get_current_pose().pose
        # # return all_close(pose, current_pose, 0.01)
        # return True

        replan = 0
        while replan < 3:
            success_percentage = 0
            while(success_percentage < 0.9):
                (plan, fraction) = self.move_group.compute_cartesian_path(
                    waypoints,  # waypoints to follow
                    0.01,       # eff_step
                    0.0)        # jump_threshold
                success_percentage = fraction

            if self.check_trajectory_plan_new(plan):
                self.move_group.execute(plan, wait=True)
                self.move_group.stop()
                return True
            else:
                print("Replanning...")
                replan += 1
        return False

    def offset_y(self, value):
        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        wpose.position.y += value   # Move along y-axis
        waypoints.append(copy.deepcopy(wpose))

        # success_percentage = 0
        # while(success_percentage < 0.9):
        #     (plan, fraction) = self.move_group.compute_cartesian_path(
        #         waypoints,  # waypoints to follow
        #         0.01,       # eff_step
        #         0.0)        # jump_threshold
        #     success_percentage = fraction

        # # Plan filtering and execution
        # plan = self.check_trajectory_plan(plan)
        # if (plan):
        #     self.move_group.execute(plan, wait=True)
        #     self.move_group.stop()
        # return True
        replan = 0
        while replan < 3:
            success_percentage = 0
            while(success_percentage < 0.9):
                (plan, fraction) = self.move_group.compute_cartesian_path(
                    waypoints,  # waypoints to follow
                    0.01,       # eff_step
                    0.0)        # jump_threshold
                success_percentage = fraction

            if self.check_trajectory_plan_new(plan):
                self.move_group.execute(plan, wait=True)
                self.move_group.stop()
                return True
            else:
                print("Replanning...")
                replan += 1
        return False

    # Direction CCW = 1.0 CW = -1.0
    def rotate_end_effector(self, direction, axis='y'):
        # Cartesian Paths
        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        q_origin = [wpose.orientation.x, wpose.orientation.y,
                    wpose.orientation.z, wpose.orientation.w]
        q_rot = quaternion_from_euler(0, direction*(0.60/180.0)*math.pi, 0)
        q_new = quaternion_multiply(q_rot, q_origin)

        wpose.orientation.x = q_new[0]
        wpose.orientation.y = q_new[1]
        wpose.orientation.z = q_new[2]
        wpose.orientation.w = q_new[3]
        waypoints.append(copy.deepcopy(wpose))

        # success_percentage = 0
        # while(success_percentage < 0.9):
        #     (plan, fraction) = self.move_group.compute_cartesian_path(
        #         waypoints,  # waypoints to follow
        #         0.01,       # eff_step
        #         0.0)        # jump_threshold
        #     success_percentage = fraction

        # Plan filtering and execution
        # plan = self.check_trajectory_plan(plan)
        # if (plan):
        #     self.move_group.execute(plan, wait=True)
        #     self.move_group.stop()

        replan = 0
        while replan < 3:
            success_percentage = 0
            while(success_percentage < 0.9):
                (plan, fraction) = self.move_group.compute_cartesian_path(
                    waypoints,  # waypoints to follow
                    0.01,       # eff_step
                    0.0)        # jump_threshold
                success_percentage = fraction

            if self.check_trajectory_plan_new(plan):
                self.move_group.execute(plan, wait=True)
                self.move_group.stop()
                return True
            else:
                print("Replanning...")
                replan += 1
        return False

    # def go_to_home(self):
    #     print("Going back to home!")
    #     self.move_group.set_named_target('home')
    #     self.move_group.go(wait=True)
    #     self.move_group.stop()

    #     # For verifying:
    #     home_joints = self.move_group.get_named_target_values('home')
    #     current_joints = self.move_group.get_current_joint_values()
    #     return all_close(home_joints, current_joints, 0.01)

    def go_to_processing_table(self):
        print("Going to processing table!")
        self.move_group.set_named_target('process_pose')
        self.move_group.go(wait=True)
        self.move_group.stop()

        # For verifying:
        process_pose_joints = self.move_group.get_named_target_values(
            'process_pose')
        current_joints = self.move_group.get_current_joint_values()
        return all_close(process_pose_joints, current_joints, 0.01)

    def go_to_spare_bay(self):
        print("Going to spare bay!")
        self.move_group.set_named_target('spare_pose')
        self.move_group.go(wait=True)
        self.move_group.stop()

        # For verifying:
        spare_bay_joints = self.move_group.get_named_target_values(
            'spare_pose')
        current_joints = self.move_group.get_current_joint_values()
        return all_close(spare_bay_joints, current_joints, 0.01)

    def change_end_effector(self, eef):
        self.current_group = eef
        print(self.current_group)
        self.move_group = moveit_commander.MoveGroupCommander(
            self.current_group)
        return True

    def change_gripper_sensor(self, sensor):
        self.current_group = sensor
        self.move_group = moveit_commander.MoveGroupCommander(
            self.current_group)
        return True

    def get_robot_current_joint(self):
        return self.move_group.get_current_joint_values()

    def get_eef_current_pose(self):
        self.current_group = "instrument_manipulator"
        return self.move_group.get_current_pose().pose

    def check_trajectory_plan(self, plan):
        # print('Current waypoints:')
        # print(len(plan.joint_trajectory.points))
        original_waypoints = [
            x.positions for x in plan.joint_trajectory.points]
        # print(original_waypoints)
        index_to_be_removed = []
        for i in range(len(plan.joint_trajectory.points)-1):
            for j in range(6):
                # check if any joints difference > 10 deg
                if (abs(plan.joint_trajectory.points[i+1].positions[j]-plan.joint_trajectory.points[i].positions[j]) > 0.174533):
                    index_to_be_removed.append(i+1)
                    break
        # print('Indices to be removed:')
        # print(index_to_be_removed)
        for index in sorted(index_to_be_removed, reverse=True):
            del plan.joint_trajectory.points[index]
        # print('Postprogressed length of waypoints:')
        # print(len(plan.joint_trajectory.points))
        # print('\n')

        return plan

    def check_trajectory_plan_new(self, plan):
        for i in range(len(plan.joint_trajectory.points)-1):
            # check if third joint difference (end state -  start joint) > 90 deg
            if (abs(plan.joint_trajectory.points[i+1].positions[2] -
                    plan.joint_trajectory.points[0].positions[2]) > 1.5708
                or
                    abs(plan.joint_trajectory.points[i+1].positions[0] -
                        plan.joint_trajectory.points[0].positions[0]) > 1.5708
                or
                    abs(plan.joint_trajectory.points[i+1].positions[4] -
                        plan.joint_trajectory.points[0].positions[4]) > 0.785398):  # 45 degree
                return False
        return True
