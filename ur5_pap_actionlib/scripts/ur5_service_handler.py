#!/usr/bin/env python
from flask import Flask, request, json, jsonify, abort
import requests
import subprocess
import os

app = Flask(__name__)

PROGRAM_MANAGER_RUNNING = False
DEBUG_COUNTER = 0


@app.route('/start/<program>', methods=['GET'])
def start(program):
    global PROGRAM_MANAGER_RUNNING
    success = False
    try:
        if program == 'ur5':
            print("Launching UR5e robot")
            subprocess.Popen(
                'gnome-terminal -- roslaunch ur_robot_driver ur5e_bringup.launch', shell=True)
            success = True
        elif program == 'camera_ai':
            # Launch PT2 camera driver and AI server
            print("Launching PT2 camera driver and AI server")
            subprocess.Popen(
                'gnome-terminal -- roslaunch vision_handler vision_handler.launch', shell=True)
            subprocess.Popen(
                'gnome-terminal -- bash /home/bigbox/all_ws/bridge5e_ws/init.sh', shell=True)
            success = True
        elif program == 'program_manager':
            # Launch PT2 camera driver and AI server
            print("Launching PT2 camera driver and AI server")
            subprocess.Popen(
                'gnome-terminal -- roslaunch vision_handler vision_handler.launch', shell=True)
            subprocess.Popen(
                'gnome-terminal -- bash /home/bigbox/all_ws/bridge5e_ws/init.sh', shell=True)
            # Launch program manager
            print("Launching program manager")
            subprocess.Popen(
                'gnome-terminal -- rosrun program_manager ur5_program_manager.py', shell=True)
            subprocess.Popen(
                'gnome-terminal -- rosrun smach_viewer smach_viewer.py', shell=True)
            success = True
            PROGRAM_MANAGER_RUNNING = True
        return jsonify({'success': success})
    except:
        abort(404)


@app.route('/kill_process', methods=['GET'])
def kill_process():
    global PROGRAM_MANAGER_RUNNING
    print("Killing program manager")
    subprocess.Popen(['rosnode kill /program_manager'],
                     shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    PROGRAM_MANAGER_RUNNING = False
    return True


@app.route('/query_process', methods=['GET'])
def query_process():
    global DEBUG_COUNTER, PROGRAM_MANAGER_RUNNING
    print("querying program status")
    # return True if ur5 process finished, else False
    # kill pt2 camera driver as well

    # debug process
    if DEBUG_COUNTER >= 5:
        PROGRAM_MANAGER_RUNNING = False
    else:
        DEBUG_COUNTER += 1
    return jsonify({'program_status': PROGRAM_MANAGER_RUNNING})


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=5002)
