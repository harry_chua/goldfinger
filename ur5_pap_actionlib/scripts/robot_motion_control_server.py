#!/usr/bin/env python

import time
import rospy
import actionlib

import geometry_msgs.msg
import ur5_pap_actionlib.msg
from ur5e_pap_controller import UR5eMotionPlanningAndControl
from ur5_gripper.srv import UR5GripperService, UR5GripperServiceRequest

import requests
import socket

IB_FLASK_IP = "http://192.168.10.104:5000/action"
UR5_DASHBOARD_SERVER_IP = "192.168.10.201"
UR5_DASHBOARD_SERVER_PORT = 29999
UR5_SOCKER_SERVER_PORT = 30002


class RobotMotionControlAction:
    feedback = ur5_pap_actionlib.msg.MotionControlFeedback()
    result = ur5_pap_actionlib.msg.MotionControlResult()

    def __init__(self):
        self.server = actionlib.SimpleActionServer(
            'ur5_control_server', ur5_pap_actionlib.msg.MotionControlAction, execute_cb=self.execute_cb, auto_start=False)
        self.server.start()

        self.robot_control = UR5eMotionPlanningAndControl()
        self.gripper_control_client = rospy.ServiceProxy(
            'ur5_gripper_server', UR5GripperService)

    # def EmergencyStop():

    def check_target_hit(self):
        touched = False
        while(not touched):
            force = rospy.wait_for_message(
                '/wrench', geometry_msgs.msg.WrenchStamped)
            if(force.wrench.force.y) > -1.0:
                self.robot_control.offset_y(0.002)
                continue
            touched = True
        return True

    def execute_cb(self, goal):
        r = rospy.Rate(100)
        # rospy.loginfo(goal)

        # Move to desired joint state
        if (goal.task == goal.MOVE_WITH_JOINT):
            desArrived = self.robot_control.go_to_joint_state(goal.goalJoints)

        # Move to desire pose
        elif (goal.task == goal.MOVE_WITH_POSE):
            desArrived = self.robot_control.go_to_pose_goal(goal.goalPose)

        # Pick instrument from bracket / spare bay
        elif (goal.task == goal.PICK):  # MOVE_WITH_POSE_OFFSET
            desArrived = False
            touched = False
            donePicking = False
            z_displacement = 0
            # Open gripper
            rospy.wait_for_service('ur5_gripper_server')
            try:
                self.gripper_control_client(
                    task=UR5GripperServiceRequest.GRIPPER_ACTION, option=UR5GripperServiceRequest.OPEN_GRIPPER)
            except rospy.ServiceException as e:
                print("Deactivate gripper service call failed: %s" % e)

            # Move above bracket or spare bay or shuttle
            if (goal.option == goal.FROM_SPARE or goal.option == goal.FROM_SHUTTLE):
                y_offset = 0
                z_offset = 0
            else:
                y_offset = -0.02
                z_offset = 0.05
            desArrived = self.robot_control.go_to_pose_goal_with_cartesian_path(
                pose=goal.goalPose, check_collision=False, y_offset=y_offset, z_offset=z_offset)

            # Check if bracket has been hit in y direction
            if (goal.option not in [goal.FROM_SPARE, goal.FROM_SHUTTLE]):
                self.check_target_hit()

            # Go down to reach scissors
            # TO DO ----->>>>> REDUCE SPEED TO REDUCE JERKY EFFECT
            if goal.option != goal.FROM_SHUTTLE:
                if desArrived:
                    rospy.wait_for_service('ur5_gripper_server')
                    # if goal.option == goal.FROM_SPARE:
                    #     ur5_dashboard.set_robot_speed(0.5)
                    try:
                        while(not touched):
                            print("\nTry picking")
                            # For empty spare bay picking check
                            print(z_displacement)
                            if (goal.option == goal.FROM_SPARE and z_displacement >= 0.055):
                                touched = False
                                break

                            read_pressure_res = self.gripper_control_client(
                                task=UR5GripperServiceRequest.GET_PRESSURE_VOLTAGE)
                            if(read_pressure_res.success):
                                # print(read_pressure_res.voltage)
                                left_sensor_value = read_pressure_res.voltage[0]
                                right_sensor_value = read_pressure_res.voltage[1]

                                # Check both pressure sensors and make rotation when needed
                                if((left_sensor_value >= 0.3 and left_sensor_value < 2.0) and (right_sensor_value >= 0.3 and right_sensor_value < 2.0)):
                                    touched = True

                                # Translation
                                elif(left_sensor_value < 0.3 and right_sensor_value < 0.3):
                                    # Move down along Z
                                    print("Moving down")
                                    if(self.robot_control.go_to_pose_goal_with_cartesian_path(
                                            pose=goal.goalPose, check_collision=True, z_offset=-0.00075)):
                                        z_displacement += 0.00075
                                    else:
                                        rospy.logerr(
                                            "Error during moving down")
                                elif(left_sensor_value >= 2.0 or right_sensor_value >= 2.0):
                                    # Move up along Z
                                    print("Moving up")
                                    self.robot_control.go_to_pose_goal_with_cartesian_path(
                                        pose=goal.goalPose, check_collision=True, z_offset=0.00055)
                                    z_displacement -= 0.00055

                                # elif(left_sensor_value >= 2.0):
                                #     # Move up along Left Sensor Z
                                #     print("Moving up left")
                                #     self.robot_control.change_gripper_sensor(
                                #         'instrument_manipulator_left')
                                #     self.robot_control.go_to_pose_goal_with_cartesian_path(
                                #         pose=goal.goalPose, check_collision=True, z_offset=0.0003)
                                #     self.robot_control.change_end_effector(
                                #         'instrument_manipulator')
                                # elif(right_sensor_value >= 2.0):
                                #     # Move up along Right Sensor Z
                                #     print("Moving up right")
                                #     self.robot_control.change_gripper_sensor(
                                #         'instrument_manipulator_right')
                                #     self.robot_control.go_to_pose_goal_with_cartesian_path(
                                #         pose=goal.goalPose, check_collision=True, z_offset=0.0003)
                                #     self.robot_control.change_end_effector(
                                #         'instrument_manipulator')

                                # Rotation
                                elif(left_sensor_value >= 0.3 and not (right_sensor_value >= 0.3)):
                                    # Rotate CCW about Left Sensor Y
                                    print("Rotating CCW")
                                    self.robot_control.change_gripper_sensor(
                                        'instrument_manipulator_left')
                                    self.robot_control.rotate_end_effector(
                                        direction=0.3)
                                    self.robot_control.change_end_effector(
                                        'instrument_manipulator')
                                elif(not (left_sensor_value >= 0.3) and right_sensor_value >= 0.3):
                                    # Rotate CW about Right Sensor Y
                                    print("Rotating CW")
                                    self.robot_control.change_gripper_sensor(
                                        'instrument_manipulator_right')
                                    self.robot_control.rotate_end_effector(
                                        direction=-0.3)
                                    self.robot_control.change_end_effector(
                                        'instrument_manipulator')

                    except rospy.ServiceException as e:
                        print("Read pressure service call failed: %s" % e)
            else:
                # For shuttle picking
                touched = True

            # Grip a pair of scissors and go back
            if desArrived and touched:
                # Resetting robot speed
                # ur5_dashboard.set_robot_speed()
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    activate_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.GRIPPER_ACTION, option=UR5GripperServiceRequest.CLOSE_GRIPPER)
                    if activate_gripper_res.success:
                        if (goal.option == goal.FROM_SPARE):
                            self.robot_control.go_to_pose_goal_with_cartesian_path(
                                pose=goal.goalPose, check_collision=False, z_offset=0)
                            donePicking = self.robot_control.offset_y(-0.18)
                        elif (goal.option == goal.FROM_SHUTTLE):
                            rospy.loginfo("Deactivating solenoid clip")
                            # ----->>>>> TO DO : INCLUDE LAY FLASK
                            # data = {"task": "stand_zero", "value": "lay"}
                            data = {'task': 'scissor_hold', 'value': 'release'}
                            released = requests.post(
                                IB_FLASK_IP, json=data).text
                            if(released):
                                time.sleep(1)
                                donePicking = self.robot_control.go_to_pose_goal_with_cartesian_path(
                                    pose=goal.goalPose, check_collision=False, z_offset=0.04)
                            else:
                                rospy.logerr(
                                    "Error during deactivating solenoid clip!")
                        else:
                            time.sleep(1)
                            donePicking = self.robot_control.go_to_pose_goal_with_cartesian_path(
                                pose=goal.goalPose, check_collision=False, gripped=True)
                except rospy.ServiceException as e:
                    print("Activate gripper service call failed: %s" % e)
            self.result.success = donePicking
            self.server.set_succeeded(self.result)

        # Place instrument in the bracket
        elif (goal.task == goal.PLACE_IN_BRACKET):
            desArrived = False
            donePlacing = False
            # Move above bracket
            desArrived = self.robot_control.go_to_pose_goal_with_cartesian_path(
                pose=goal.goalPose, check_collision=False, y_offset=-0.008, z_offset=0.175)
            # Release instrument
            if desArrived:
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    deactivate_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.GRIPPER_ACTION, option=UR5GripperServiceRequest.OPEN_GRIPPER)
                    if deactivate_gripper_res.success:
                        self.robot_control.go_to_pose_goal_with_cartesian_path(
                            pose=goal.goalPose, check_collision=False, y_offset=-0.005, z_offset=0.25)
                        donePlacing = self.robot_control.go_to_processing_table()
                except rospy.ServiceException as e:
                    print("Deactivate gripper service call failed: %s" % e)
            self.result.success = donePlacing
            self.server.set_succeeded(self.result)

        # Place instrument in the shuttle
        elif (goal.task == goal.PLACE_IN_SHUTTLE):
            desArrived = False
            touched = False
            donePlacing = False

            # Move to shuttle
            desArrived = self.robot_control.go_to_pose_goal_with_cartesian_path(
                pose=goal.goalPose, check_collision=False, z_offset=0)

            # if desArrived:
            #     if(self.check_target_hit()):
            #         touched = self.robot_control.go_to_pose_goal_with_cartesian_path(
            #             pose=goal.goalPose, check_collision=False, z_offset=-0.01)
            #     else:
            #         rospy.logerr("Error during shuttle wall searching!")
            # else:
            #     rospy.logerr("Error during shuttle navigating!")

            # Lock shuttle
            # if desArrived and touched:
            # rospy.loginfo("Activating solenoid clip")
            # data = {'task': 'scissor_hold', 'value': 'grip'}
            # x = requests.post(IB_FLASK_IP, json=data).text

            # Release instrument
            if desArrived:
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    deactivate_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.GRIPPER_ACTION, option=UR5GripperServiceRequest.OPEN_GRIPPER)
                    if deactivate_gripper_res.success:
                        time.sleep(1)
                        rospy.loginfo("Activating solenoid clip")
                        data = {'task': 'scissor_hold', 'value': 'grip'}
                        released = requests.post(IB_FLASK_IP, json=data).text
                        # data = {'task': 'stand_one', 'value': 'lay'}
                        # released = requests.post(IB_FLASK_IP, json=data).text
                        if(released):
                            donePlacing = self.robot_control.go_to_processing_table()
                        else:
                            rospy.logerr(
                                "Error during activating solenoid clip!")
                except rospy.ServiceException as e:
                    print("Deactivate gripper service call failed: %s" % e)
            self.result.success = donePlacing
            self.server.set_succeeded(self.result)

        # Place instrument in the bin
        elif (goal.task == goal.PLACE_IN_BIN):
            desArrived = False
            donePlacing = False

            # Move to bin
            desArrived = self.robot_control.go_to_pose_goal_with_cartesian_path(
                pose=goal.goalPose, check_collision=False, z_offset=0)

            # Release instrument
            if desArrived:
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    deactivate_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.GRIPPER_ACTION, option=UR5GripperServiceRequest.OPEN_GRIPPER)
                    if deactivate_gripper_res.success:
                        donePlacing = self.robot_control.go_to_spare_bay()
                except rospy.ServiceException as e:
                    print("Deactivate gripper service call failed: %s" % e)
            self.result.success = donePlacing
            self.server.set_succeeded(self.result)

        # Move to default home position
        # elif (goal.task == goal.MOVE_TO_HOME):
        #     desArrived = self.robot_control.go_to_home()
        #     if desArrived:
        #         self.result.desArrived = True
        #     else:
        #         self.result.desArrived = False
        #     self.server.set_succeeded(self.result)

        # Move to processing table
        elif (goal.task == goal.MOVE_TO_PROCESSING_TABLE):
            self.result.desArrived = self.robot_control.go_to_processing_table()
            # if desArrived:
            #     self.result.desArrived = True
            # else:
            #     self.result.desArrived = False
            self.server.set_succeeded(self.result)

        # Move to spare bay
        elif (goal.task == goal.MOVE_TO_SPARE_BAY):
            desArrived = self.robot_control.go_to_spare_bay()
            if desArrived:
                self.result.desArrived = True
            else:
                self.result.desArrived = False
            self.server.set_succeeded(self.result)

        # Swith manipulator from 'instrument' to 'locker', vice versa
        elif (goal.task == goal.CHANGE_EEF):
            if(goal.option == goal.INSTRUMENT_GRIPPER):
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    change_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.CHANGE_GRIPPER, option=UR5GripperServiceRequest.INSTRUMENT_GRIPPER)
                    print(change_gripper_res)
                except rospy.ServiceException as e:
                    print("Change gripper service call failed: %s" % e)
                changed = self.robot_control.change_end_effector(
                    'instrument_manipulator')
            elif(goal.option == goal.LOCKER_GRIPPER):
                rospy.wait_for_service('ur5_gripper_server')
                try:
                    change_gripper_res = self.gripper_control_client(
                        task=UR5GripperServiceRequest.CHANGE_GRIPPER, option=UR5GripperServiceRequest.LOCKER_GRIPPER)
                    print(change_gripper_res)
                except rospy.ServiceException as e:
                    print("Change gripper service call failed: %s" % e)
                changed = self.robot_control.change_end_effector(
                    'locker_manipulator')
            if(change_gripper_res.success and changed):
                self.result.success = True
            else:
                self.result.success = False
            self.server.set_succeeded(self.result)

        elif (goal.task == goal.GET_POSE):
            print(self.robot_control.get_eef_current_pose())

        elif (goal.task == goal.GET_JOINT):
            print(self.robot_control.get_robot_current_joint())

        # Watch Dog time...
        self.feedback.currentPose = [0, 1, 2, 3]
        self.server.publish_feedback(self.feedback)


class DashboardServer:
    def __init__(self):
        connected = False
        ''' Motion Server will try to start up the robot 10 times '''
        for i in range(10):
            try:
                self.ur5_socket = socket.socket(
                    socket.AF_INET, socket.SOCK_STREAM)
                self.ur5_socket.connect(
                    (UR5_DASHBOARD_SERVER_IP, UR5_DASHBOARD_SERVER_PORT))
                rospy.loginfo(self.ur5_socket.recv(2048))
                connected = True
                break
            except:
                rospy.logerr('UR5 connection failed, retrying...')
                time.sleep(1)
        if not connected:
            rospy.logerr(
                'Could not establish connection to UR5, please check if it is powered on!')
            # return None
        else:
            if self.dashboard_command('power on') and self.dashboard_command('brake release') and self.dashboard_command('load external_control_dell.urp'):
                print('Waiting 15 secs for brake release')
                time.sleep(15)
                self.dashboard_command('play')
                # Set default robot speed
                self.set_robot_speed()
                # return self.ur5_socket
                # Handle false statement
            else:
                rospy.logerr('Error during powering on the robot')
                # return None

    def dashboard_command(self, command):
        ''' Dashboard Server API call '''
        if command == 'power off':
            rospy.loginfo('Powering off robot!')

        try:
            self.ur5_socket.send(command + '\n')
            print(self.ur5_socket.recv(2048))
            # if (command     )
            return True
        except self.ur5_socket.timeout as err:
            rospy.logerr(err)
            return False

    def set_robot_speed(self, speed=0.75):
        ''' Socket Server API call '''
        speed_socket = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)
        speed_socket.connect(
            (UR5_DASHBOARD_SERVER_IP, UR5_SOCKER_SERVER_PORT))
        print('Setting robot speed to ', speed)
        speed_socket.send('set speed ' + str(speed) + '\n')
        return True


if __name__ == '__main__':
    global ur5_dashboard
    rospy.init_node('ur5_control_server')
    ur5_dashboard = DashboardServer()
    if ur5_dashboard:
        server = RobotMotionControlAction()
        rospy.loginfo('UR5e Robot Motion Control Server READY!!!')
    # rospy.on_shutdown(ur5_dashboard.dashboard_command('power off'))
    rospy.spin()
