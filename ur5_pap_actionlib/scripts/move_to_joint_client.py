#!/usr/bin/env python

import rospy
import actionlib
import ur5_pap_actionlib.msg
import math


def moveToJoint_client():
    client = actionlib.SimpleActionClient(
        'ur5_control_server', ur5_pap_actionlib.msg.MotionControlAction)

    client.wait_for_server()

    goal = ur5_pap_actionlib.msg.MotionControlGoal(
        task=ur5_pap_actionlib.msg.MotionControlGoal.MOVE_WITH_JOINT,
        goalJoints=[0.3927, -2.3562, -1.1781, -1.1781, 1.5707, 0])

    client.send_goal(goal)

    client.wait_for_result()

    print client.get_result()


if __name__ == '__main__':
    try:
        rospy.init_node('move_to_joint_client')
        result = moveToJoint_client()
    except rospy.ROSInterruptException:
        print 'byebye'
