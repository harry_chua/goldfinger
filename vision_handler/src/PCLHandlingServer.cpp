#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <opencv2/aruco.hpp>

#include "vision_handler/ArucoMarkerMidPt.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>

#include <tf2_ros/transform_listener.h>
#include <tf2_ros/message_filter.h>
#include <message_filters/subscriber.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <math.h>

#include <rc_common_msgs/Trigger.h>
#include <vision_handler/PCLHandlerAction.h>
#include <actionlib/server/simple_action_server.h>

namespace PCL
{
    class PCLHandlingServer
    {
    private:
        ros::NodeHandle nh;

        actionlib::SimpleActionServer<vision_handler::PCLHandlerAction> PCLServer;
        vision_handler::PCLHandlerResult result;

        std::uint32_t u;
        std::uint32_t v;

        // ros::ServiceClient aruco_detector_client;
        // vision_handler::ArucoMarkerMidPt get_instr_coor_srv;

        ros::ServiceClient rc_visard_client;
        rc_common_msgs::Trigger pc_capture_srv;

        ros::Subscriber pc_sub;

        tf2_ros::Buffer tfBuffer;
        tf2_ros::TransformListener tf2;

        geometry_msgs::PoseStamped targetPt2World;
        bool POSESTAMPEDREADY = false;

    public:
        PCLHandlingServer() : PCLServer(nh, "pcl_handling_server", boost::bind(&PCL::PCLHandlingServer::executeCB, this, _1), false),
                            //   aruco_detector_client(nh.serviceClient<vision_handler::ArucoMarkerMidPt>("/aruco_detector/instrument_coor")),
                              rc_visard_client(nh.serviceClient<rc_common_msgs::Trigger>("/rc_visard_driver/depth_acquisition_trigger")),
                              pc_sub(nh.subscribe("/stereo/points2", 1, &PCL::PCLHandlingServer::pointCloudCB, this)),
                              tf2(tfBuffer)
        {
            ROS_INFO_STREAM("Camera trigger service READY!");
            PCLServer.start();
        };

        ~PCLHandlingServer(){};

        void executeCB(const vision_handler::PCLHandlerGoalConstPtr &goal)
        {
            ROS_INFO_STREAM(goal->task);
            POSESTAMPEDREADY = false;
            if (goal->task == goal->GET_INSTRUMENT_POSE)
            {
                u = goal->u;
                v = goal->v;
                if (rc_visard_client.call(pc_capture_srv))
                {
                    ROS_INFO_STREAM(pc_capture_srv.response);
                    ROS_INFO_STREAM("Image captured!");
                }
                else
                {
                    ROS_ERROR("Failed to call service 'depth_acquisition_trigger'");
                }
            }
            while (!POSESTAMPEDREADY)
            {
            }
            result.targetPoseStamped = targetPt2World;
            result.success = true;
            // std::cout << result << std::endl;
            PCLServer.setSucceeded(result);
        };

        void pointCloudCB(const sensor_msgs::PointCloud2ConstPtr &pcd_msg)
        {
            // vision_handler::ArucoMarkerMidPtResponse midPt;
            // if (aruco_detector_client.call(get_instr_coor_srv))
            // {
            //     midPt = get_instr_coor_srv.response;
            //     // std::cout << midPt << std::endl;
            // }
            // else
            // {
            //     ROS_ERROR("Failed to call service '/aruco_detector/instrument_coor'");
            // }

            pcl::PointCloud<pcl::PointXYZ> pcd;
            pcl::fromROSMsg(*pcd_msg, pcd);
            // pcl::PointXYZ p1 = pcd.at(midPt.instrument_coor[0], midPt.instrument_coor[1]);
            pcl::PointXYZ p1 = pcd.at(u, v);
            std::cout << p1 << std::endl;

            geometry_msgs::PoseStamped targetPt2Cam;
            targetPt2Cam.header.frame_id = "camera";
            targetPt2Cam.header.stamp = ros::Time::now();
            targetPt2Cam.pose.position.x = p1.x;
            targetPt2Cam.pose.position.y = p1.y;
            targetPt2Cam.pose.position.z = p1.z;

            tf2::Quaternion quat;
            quat.setRPY(0, 0, (-90) * M_PI / 180);
            targetPt2Cam.pose.orientation.x = quat.x();
            targetPt2Cam.pose.orientation.y = quat.y();
            targetPt2Cam.pose.orientation.z = quat.z();
            targetPt2Cam.pose.orientation.w = quat.w();

            PCL::PCLHandlingServer::transform_frame(&targetPt2Cam, &targetPt2World);
            // std::cout << targetPt2World << std::endl;
            POSESTAMPEDREADY = true;
        };

        void transform_frame(const geometry_msgs::PoseStamped *targetPt2Cam, geometry_msgs::PoseStamped *targetPt2World)
        {
            geometry_msgs::TransformStamped cameraTransformStamped;
            try
            {
                cameraTransformStamped = tfBuffer.lookupTransform("world", "camera", ros::Time(0));
                // std::cout << cameraTransformStamped << std::endl;

                tfBuffer.transform(*targetPt2Cam, *targetPt2World, "world");
            }
            catch (tf2::TransformException &ex)
            {
                ROS_WARN("%s", ex.what());
                ros::Duration(1.0).sleep();
            }
        }
    };
} // namespace PCL

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pcl_handling_server");
    PCL::PCLHandlingServer server;
    ROS_INFO_STREAM("PCL Handling Server READY!");
    ros::spin();
    return 0;
}