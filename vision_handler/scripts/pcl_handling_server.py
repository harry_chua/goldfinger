#!/usr/bin/env python

import rospy
import actionlib
import vision_handler.msg
from rc_common_msgs.srv import *


class PCLAction:
    def __init__(self):
        self.server = actionlib.SimpleActionServer(
            'pcl_handling_server', vision_handler.msg.PCLHandlerAction, execute_cb=self.execute_cb, auto_start=False)

        self.server.start()

        self.rc_visard_client = rospy.ServiceProxy(
            '/rc_visard_driver/depth_acquisition_trigger', Trigger)
        self.point

    def execute_cb(self, goal):
        r = rospy.Rate(100)

        rospy.loginfo(goal)

        if (goal.task == goal.CAPTURE_IMAGE):
            rospy.wait_for_service(
                '/rc_visard_driver/depth_acquisition_trigger')
            try:
                depth = self.rc_visard_client()
                rospy.loginfo(depth)
                self.server.set_succeeded(depth)
            except rospy.ServiceException as e:
                print("Aruco marker count service call failed: %s" % e)


if __name__ == '__main__':
    rospy.init_node('pcl_handling_server')
    server = PCLAction()
    rospy.loginfo('PCL Handling Server READY!')
    rospy.spin()
