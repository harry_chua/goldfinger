#!/usr/bin/env python

import time
from cv_bridge.core import CvBridge
import rospy
import cv2
import sensor_msgs.msg

from vision_handler.srv import ArucoMarkerMidPt, ArucoMarkerMidPtResponse, BracketZoneDetector, BracketZoneDetectorResponse


def get_boundary(corners, index_list):
    boundary_x, boundary_y = [], []
    if len(index_list) >= 3:
        for i in index_list:
            sub_corners = corners[i].reshape(4, 2)
            (topLeft, topRight, bottomRight, bottomLeft) = sub_corners
            topLeft = (int(topLeft[0]), int(topLeft[1]))
            topRight = (int(topRight[0]), int(topRight[1]))
            bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
            bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
            cx = int((topLeft[0]+bottomRight[0]) / 2.0)
            cy = int((topLeft[1]+bottomRight[1]) / 2.0)
            boundary_x.append(cx)
            boundary_y.append(cy)
        # print(boundary_x, boundary_y)
        min_x = min(boundary_x)
        max_x = max(boundary_x)
        min_y = min(boundary_y)
        max_y = max(boundary_y)
        return [min_x, max_x, min_y, max_y]
    return []


def draw_result(zone, cv_image, index):
    # Draw the bounding box of the ArUco detection
    cv2.line(cv_image, (zone[0], zone[2]), (zone[1], zone[2]), (0, 255, 0), 2)
    cv2.line(cv_image, (zone[1], zone[2]), (zone[1], zone[3]), (0, 255, 0), 2)
    cv2.line(cv_image, (zone[1], zone[3]), (zone[0], zone[3]), (0, 255, 0), 2)
    cv2.line(cv_image, (zone[0], zone[3]), (zone[0], zone[2]), (0, 255, 0), 2)

    # Calculate and draw the center of the ArUco marker
    center_x = int((zone[0] + zone[1]) / 2.0)
    center_y = int((zone[2] + zone[3]) / 2.0)
    cv2.circle(cv_image, (center_x, center_y), 4, (0, 0, 255), -1)

    # Draw the ArUco marker ID on the video frame
    # The ID is always located at the top_left of the ArUco marker
    cv2.putText(cv_image, index,
                (zone[0], zone[2] - 15),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5, (0, 255, 0), 2)


class ArucoMarkerDetector:
    def __init__(self):
        self.aruco_detector_server = rospy.Service(
            '/aruco_detector/shuttle_coor', ArucoMarkerMidPt, self.handleShuttlePoint)
        self.aruco_detector_server = rospy.Service(
            '/aruco_detector/bracket_zone', BracketZoneDetector, self.handleBracketZone)

        self.arucoDict = cv2.aruco.Dictionary_get(
            cv2.aruco.DICT_ARUCO_ORIGINAL)
        self.arucoParams = cv2.aruco.DetectorParameters_create()

    def handleShuttlePoint(self, req):
        detected = False
        while(not detected):
            image = rospy.wait_for_message('/stereo/left/image_rect_color',
                                           sensor_msgs.msg.Image)
            cv_image = CvBridge().imgmsg_to_cv2(image, "bgr8")
            (corners, ids, rejected) = cv2.aruco.detectMarkers(
                cv_image, self.arucoDict, parameters=self.arucoParams)

            ids = ids.flatten()
            index84 = [i for i, e in enumerate(ids) if e == 84]
            markers_midpt = []
            if len(index84) == 2:
                for i in index84:
                    sub_corners = corners[i].reshape(4, 2)
                    (topLeft, topRight, bottomRight, bottomLeft) = sub_corners
                    topLeft = (int(topLeft[0]), int(topLeft[1]))
                    topRight = (int(topRight[0]), int(topRight[1]))
                    bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
                    bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
                    cx = int((topLeft[0]+bottomRight[0]) / 2.0)
                    cy = int((topLeft[1]+bottomRight[1]) / 2.0)
                    markers_midpt.append([cx, cy])
                detected = True
            else:
                rospy.loginfo("Retrying...")

        print(markers_midpt)
        # picking_center_x = int((boundary_x[0]+boundary_x[1])/2.0)
        # picking_center_y = int((boundary_y[0]+boundary_y[1])/2.0)
        # mid_point = [picking_center_x, picking_center_y]
        # print(mid_point)

        # Display the resulting frame
        cv2.circle(cv_image, (markers_midpt[0][0], markers_midpt[0][1]),
                   4, (0, 0, 255), -1)
        cv2.circle(cv_image, (markers_midpt[1][0], markers_midpt[1][1]),
                   4, (0, 0, 255), -1)
        cv2.imwrite('./shuttle_midpt.jpg', cv_image)

        return ArucoMarkerMidPtResponse(markers_midpt[0], markers_midpt[1])

    def handleBracketZone(self, req):
        all_zones_detected = False
        while(not all_zones_detected):
            image = rospy.wait_for_message('/stereo/left/image_rect_color',
                                           sensor_msgs.msg.Image)
            cv_image = CvBridge().imgmsg_to_cv2(image, "bgr8")
            (corners, ids, rejected) = cv2.aruco.detectMarkers(
                cv_image, self.arucoDict, parameters=self.arucoParams)
            ids = ids.flatten()

            # Zone A
            index582 = [i for i, e in enumerate(ids) if e == 582]
            zone_a = get_boundary(corners, index582)
            if zone_a:
                draw_result(zone_a, cv_image, 'A')
            print("Zone A: ", zone_a)

            # Zone B
            index116 = [i for i, e in enumerate(ids) if e == 116]
            zone_b = get_boundary(corners, index116)
            if zone_b:
                draw_result(zone_b, cv_image, 'B')
            print("Zone B: ", zone_b)

            # Zone C
            index373 = [i for i, e in enumerate(ids) if e == 373]
            zone_c = (get_boundary(corners, index373))
            if zone_c:
                draw_result(zone_c, cv_image, 'C')
            print("Zone C: ", zone_c)

            if(zone_a and zone_b and zone_c):
                all_zones_detected = True
            else:
                time.sleep(1)

        # Display the resulting frame
        cv2.imwrite('./zone.jpg', cv_image)

        # cv2.imshow('image', cv_image)
        # if cv2.waitKey(1) & 0xFF == ord('q'):
        # break

        return BracketZoneDetectorResponse(zone_a, zone_b, zone_c)


if __name__ == '__main__':
    rospy.init_node('aruco_detector')
    detector = ArucoMarkerDetector()
    rospy.loginfo('Aruco marker detector READY!')
    rospy.spin()
