#!/usr/bin/env python
from time import sleep
from ur5_gripper.srv import *
import rospy
import os
from dynamixel_sdk import *
from time import sleep
import time
from ur_msgs.msg import IOStates

# dynamixel controller


class DynamixelController():
    def __init__(self):
        # Control table address
        # Control table address is different in Dynamixel model
        self.ADDR_PRO_TORQUE_ENABLE = 64
        self.ADDR_PRO_GOAL_POSITION = 116
        self.ADDR_PRO_PRESENT_POSITION = 132

        # Protocol version
        # See which protocol version is used in the Dynamixel
        self.PROTOCOL_VERSION = 2.0

        # Default setting
        self.DXL_ID = 1                 # Dynamixel ID : 1
        self.BAUDRATE = 57600             # Dynamixel default baudrate : 57600
        # Check which port is being used on your controller, Linux: "/dev/ttyUSB0"
        self.DEVICENAME = '/dev/ttyUSB0'

        # Value for enabling the torque Check e-manual about the range of the Dynamixel you use.)
        self.TORQUE_ENABLE = 1

        self.portHandler = PortHandler(self.DEVICENAME)
        self.packetHandler = PacketHandler(self.PROTOCOL_VERSION)

        # Open port and baudrate
        if self.portHandler.openPort() and self.portHandler.setBaudRate(self.BAUDRATE):
            print("Succeeded to open the port and change the baudrate")
        else:
            print("Failed to open the port")

        # Enable Dynamixel Torque
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(
            self.portHandler, self.DXL_ID, self.ADDR_PRO_TORQUE_ENABLE, self.TORQUE_ENABLE)
        if dxl_comm_result != COMM_SUCCESS and dxl_error != 0:
            print("Dynamixel has been successfully connected")

    def move_dynamixel(self, pos):
        dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(
            self.portHandler, self.DXL_ID, self.ADDR_PRO_GOAL_POSITION, pos)
        if dxl_comm_result != COMM_SUCCESS and dxl_error != 0:
            print("moving task sent successfully")
        sleep(1.3)
        dxl_present_position, dxl_comm_result, dxl_error = self.packetHandler.read4ByteTxRx(
            self.portHandler, self.DXL_ID, self.ADDR_PRO_PRESENT_POSITION)
        if abs(dxl_present_position - pos) <= 10:
            print("dynamixel has arrvived at desination")
            return True
        else:
            return False


rospy.init_node('ur5_gripper_server')
r = rospy.Rate(10)  # 10hz

CURRENT_GRIPPER = "locker_gripper"


class UR5GripperServer(object):
    def __init__(self):
        self.service = rospy.Service(
            'ur5_gripper_server', UR5GripperService, self.execute)

    def execute(self, req):
        global CURRENT_GRIPPER
        print("service called")
        response = UR5GripperServiceResponse()

        # gripper changing task
        if req.task == req.CHANGE_GRIPPER:
            if req.option == req.INSTRUMENT_GRIPPER:
                CURRENT_GRIPPER = "instrument_gripper"
                response.success = True
            elif req.option == req.LOCKER_GRIPPER:
                CURRENT_GRIPPER = "locker_gripper"
                response.success = True

        # gripper action task
        elif req.task == req.GRIPPER_ACTION:
            if req.option == req.OPEN_GRIPPER:

                if CURRENT_GRIPPER == "instrument_gripper":
                    # release the scrssor by deactivating the io
                    print("opening instrument gripper")
                    os.system(
                        "rosservice call /ur_hardware_interface/set_io 1 0 0")
                    response.success = True

                elif CURRENT_GRIPPER == "locker_gripper":
                    # dynamixel to position 120
                    print("moving dynamixel to 120")
                    if dynamixel.move_dynamixel(120):
                        response.success = True
                    else:
                        response.success = False

            if req.option == req.CLOSE_GRIPPER:
                # open the scrssor by activating the io
                if CURRENT_GRIPPER == "instrument_gripper":
                    print("closing instrument gripper")
                    os.system(
                        "rosservice call /ur_hardware_interface/set_io 1 0 1")
                    response.success = True

                elif CURRENT_GRIPPER == "locker_gripper":
                    # dynamixel to position 2168
                    print("moving dynamixel to 2168")
                    if dynamixel.move_dynamixel(2168):
                        response.success = True
                    else:
                        response.success = False

        elif req.task == req.GET_PRESSURE_VOLTAGE:
            # to read ur analg io and return 0-3v based on pressure sensor
            print("Getting pressure sensor reading....")
            res = rospy.wait_for_message(
                '/ur_hardware_interface/io_states', IOStates)
            response.voltage = [
                res.analog_in_states[0].state, res.analog_in_states[1].state]
            response.success = True
        print(response)
        return response


dynamixel = DynamixelController()
server = UR5GripperServer()

while not rospy.is_shutdown():
    r.sleep()
