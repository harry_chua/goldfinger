from geometry_msgs.msg import Pose


def list_to_pose(input_list):
    pose = Pose()
    pose.position.x = input_list[0]
    pose.position.y = input_list[1]
    pose.position.z = input_list[2]
    pose.orientation.x = input_list[3]
    pose.orientation.y = input_list[4]
    pose.orientation.z = input_list[5]
    pose.orientation.w = input_list[6]
    return pose


def get_zone(coor, all_zone):
    """
    Get the zone (A/B/C) where the coordinates given are located.
    """
    for i in range(3):
        if (coor[0] > all_zone[i][0] and
                coor[0] < all_zone[i][1] and
                coor[1] > all_zone[i][2] and
                coor[1] < all_zone[i][3]
                ):
            return all_zone[i]
    return None


def filter_zone(ai_data_list, zone):
    """
    Return the respective data filtered by the specific zone.
    """
    for x in ai_data_list:
        if (x.u > zone[0] and
                x.u < zone[1] and
                x.v > zone[2] and
                x.v < zone[3]
                ):
            return x
    return None


def within_zone(coor, zone):
    """
    Check if the coordinates are in the given zone.
    """
    if (coor[0] > zone[0] and
            coor[0] < zone[1] and
            coor[1] > zone[2] and
            coor[1] < zone[3]
            ):
        return True
    return False
