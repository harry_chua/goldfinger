#!/usr/bin/env python
import rospy
import smach
import smach_ros
import actionlib

from geometry_msgs.msg import PoseStamped

import ai_vision_server.msg
from ur5_gripper.srv import *
from vision_handler.msg import *
from ur5_pap_actionlib.msg import *

import requests
from flask import json
from time import sleep

from constants import *

motion_client = None
pcl_client = None
ai_client = None

SHUTTLE = 1  # outside shuttle id


def initialization():
    """connect to all action server here"""
    global motion_client, pcl_client, ai_client

    motion_client = actionlib.SimpleActionClient(
        'ur5_control_server', MotionControlAction)
    motion_client.wait_for_server()
    rospy.loginfo("Motion server connected")

    pcl_client = actionlib.SimpleActionClient(
        'pcl_handling_server', PCLHandlerAction)
    pcl_client.wait_for_server()
    rospy.loginfo("PCL server connected")

    ai_client = actionlib.SimpleActionClient(
        'ai_server', ai_vision_server.msg.AIServerAction)
    ai_client.wait_for_server()
    rospy.loginfo("AI vision server connected")

    rospy.loginfo("All server connected!")


def get_coor(tar_des):
    ai_goal = ai_vision_server.msg.AIServerGoal()
    ai_goal.task = ai_vision_server.msg.AIServerGoal.PICK
    ai_goal.target_des = tar_des
    ai_client.send_goal(ai_goal)
    ai_client.wait_for_result()
    ai_result = ai_vision_server.msg.AIServerResult()
    ai_result = ai_client.get_result()
    if ai_result.success:
        print(ai_result)
        return ai_result.ai_result.u, ai_result.ai_result.v
    else:
        rospy.logerr("Error during getting coor")


def get_pose(u, v):
    return_pose = None
    # pls verify this ar marker getting action call
    pcl_goal = PCLHandlerGoal()  # get center of ar marker
    pcl_goal.task = pcl_goal.GET_INSTRUMENT_POSE
    pcl_goal.u = u
    pcl_goal.v = v
    pcl_client.send_goal(pcl_goal)
    pcl_client.wait_for_result()
    pcl_result = PCLHandlerResult()
    pcl_result = pcl_client.get_result()
    if pcl_result.success:
        return_pose = pcl_result.targetPoseStamped
        # print(return_pose.pose)
    else:
        rospy.logerr("Error during getting pose")
    return return_pose.pose

    # pose = PoseStamped()
    # pose.pose.position.x = 0.631976663843
    # pose.pose.position.y = -0.0533982473145
    # pose.pose.position.z = 0.764239455609
    # pose.pose.orientation.x = 0.0575617674039
    # pose.pose.orientation.y = -0.998328572765
    # pose.pose.orientation.z = 0.0036369695301
    # pose.pose.orientation.w = 0.00367099258312
    # return pose.pose


def list_to_pose(input_list):
    pose = PoseStamped()
    pose.pose.position.x = input_list[0]
    pose.pose.position.y = input_list[1]
    pose.pose.position.z = input_list[2]
    pose.pose.orientation.x = input_list[3]
    pose.pose.orientation.y = input_list[4]
    pose.pose.orientation.z = input_list[5]
    pose.pose.orientation.w = input_list[6]
    return pose.pose


def open_locker(bracket_coor):
    motion_goal = MotionControlGoal()  # gripper changing task
    # include change gripper in motion server, and code this in motion server such that the manipulator group will chg accordingly
    motion_goal.option = motion_goal.CHANGE_GRIPPER
    motion_goal.gripper = motion_goal.LOCKER_GRIPPER
    motion_client.send_goal(motion_goal)
    motion_client.wait_for_result()
    if motion_client.get_result().success:
        if motion_client.get_result().success:
            motion_goal = MotionControlGoal()
            motion_goal.option = motion_goal.MOVE_TO_PROCESSING_TABLE
            motion_client.send_goal(motion_goal)
            motion_client.wait_for_result()
            if motion_client.get_result().success:
                motion_goal = MotionControlGoal()
                motion_goal.option = motion_goal.LOCKER_OPENING
                motion_goal.target_pose = list_to_pose(bracket_coor)
                motion_client.send_goal(motion_goal)
                motion_client.wait_for_result()
                if motion_client.get_result().success:
                    motion_goal = MotionControlGoal()
                    motion_goal.option = motion_goal.MOVE_TO_PROCESSING_TABLE
                    motion_client.send_goal(motion_goal)
                    motion_client.wait_for_result()
                    if motion_client.get_result().success:
                        return True


def pick_and_place(pick_joint, pick_pose_joint, place_joint, place_coor):
    motion_goal = MotionControlGoal()  # gripper changing task
    # include change gripper in motion server, and code this in motion server such that the manipulator group will chg accordingly
    motion_goal.task = motion_goal.CHANGE_EEF
    motion_goal.option = motion_goal.INSTRUMENT_GRIPPER
    motion_client.send_goal(motion_goal)
    motion_client.wait_for_result()
    if motion_client.get_result().success:
        motion_goal = MotionControlGoal()  # homing task
        if pick_joint == "ib":
            motion_goal.task = motion_goal.MOVE_TO_PROCESSING_TABLE
        elif pick_joint == "sb":
            motion_goal.task = motion_goal.MOVE_TO_SPARE_BAY
        motion_client.send_goal(motion_goal)
        motion_client.wait_for_result()
        if motion_client.get_result().desArrived:
            # picking task, implememnt z down search function here in motion server
            motion_goal = MotionControlGoal()
            # code this PICK program flow
            motion_goal.task = motion_goal.PICK
            if pick_joint == "sb":
                motion_goal.option = motion_goal.FROM_SPARE
                motion_goal.goalJoints = pick_pose_joint
            # elif pick_joint == "sb":
            #     motion_goal.option = motion_goal.MOVE_TO_SPARE_BAY
            else:
                motion_goal.goalPose = pick_pose_joint
            motion_client.send_goal(motion_goal)
            motion_client.wait_for_result()
            if motion_client.get_result().success:
                motion_goal = MotionControlGoal()  # homing task
                if place_joint == "ib":
                    motion_goal.task = motion_goal.MOVE_TO_PROCESSING_TABLE
                elif place_joint == "sb":
                    motion_goal.task = motion_goal.MOVE_TO_SPARE_BAY
                motion_client.send_goal(motion_goal)
                motion_client.wait_for_result()
                if motion_client.get_result().success:
                    motion_goal = MotionControlGoal()  # placing task
                    # code this PLACE program flow
                    motion_goal.task = motion_goal.PLACE_IN_SHUTTLE
                    motion_goal.goalPose = list_to_pose(place_coor)
                    motion_client.send_goal(motion_goal)
                    motion_client.wait_for_result()
                    if motion_client.get_result().success:
                        rospy.loginfo("Transfer successfully")
                        return True
                    else:
                        rospy.logerr("Error during placing")
                        return False
                else:
                    rospy.logerr("Error during returning")
                    return False
            else:
                rospy.logerr("Error during picking")
                return False
        else:
            rospy.logerr("Error during homing")
            return False
    else:
        rospy.logerr("Error during gripper changing")
        return False
# SMACH CLASSES ##############################################


class WaitForStart(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['sb', 'ib', 'fail'])

    def execute(self, userdata):
        print("Type sb for sparebay demo, ib for inspection bay loading demo")
        start = raw_input()
        if start == "sb":
            return 'sb'
        elif start == "ib":
            return 'ib'
        else:
            return 'fail'


class TransferToIB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])

    def execute(self, userdata):
        u, v = get_coor(ai_vision_server.msg.AIServerGoal.UR5_TOOL)

        """get pose with ar marker, proceed to pick a ring insturment, and transfer to inpsection bay"""
        bracket_pose = get_pose(u, v)
        print(bracket_pose)
        if bracket_pose:  # bracket coor will return None if ai or pcl server is problematic
            rospy.loginfo(
                "activating scissor holder ans deactivating solenoid clip")
            if SHUTTLE == 0:
                data = {'task': 'stand_zero', 'value': 'stand'}
            elif SHUTTLE == 1:
                data = {'task': 'stand_one', 'value': 'stand'}
            x = requests.post(IB_FLASK_IP, json=data).text
            data = {'task': 'scissor_hold', 'value': 'release'}
            x = requests.post(IB_FLASK_IP, json=data).text
            # if open_locker(bracket_coor):
            if pick_and_place("ib", bracket_pose, "ib", SHUTTLE_COOR):
                rospy.loginfo(
                    "succussfully transfer instrument to inpsection bay shuttle")
                return 'success'
            else:
                rospy.logerr("error during trasnferring")
                return 'fail'
            # else:
            # rospy.logerr("error during locker opening")
            # return 'fail'
        else:
            rospy.logerr(
                "bracket coor return none, something wrong with ai / pcl")
            return 'fail'


class ActivateIB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])

    def execute(self, userdata):
        global SHUTTLE
        """send a series of flask command to inspection bay"""
        # delete this during full run
        if SHUTTLE == 0:
            data = {'task': 'station_state', 'value': 'one'}
        elif SHUTTLE == 1:
            data = {'task': 'station_state', 'value': 'zero'}
        x = requests.post(IB_FLASK_IP, json=data).text
        if SHUTTLE == 0:
            data = {'task': 'stand_zero', 'value': 'lay'}
        elif SHUTTLE == 1:
            data = {'task': 'stand_one', 'value': 'lay'}
        x = requests.post(IB_FLASK_IP, json=data).text
        data = {'task': 'scissor_hold', 'value': 'release'}
        x = requests.post(IB_FLASK_IP, json=data).text

        raw_input("initialization copleted, enter after loaded instrument to grip")
        data = {'task': 'scissor_hold', 'value': 'grip'}
        x = requests.post(IB_FLASK_IP, json=data).text
        # delete this during full run end
        # send shuttle in and activate oil spray
        if SHUTTLE == 0:
            data = {'task': 'stand_zero', 'value': 'stand'}
        elif SHUTTLE == 1:
            data = {'task': 'stand_one', 'value': 'stand'}
        x = requests.post(IB_FLASK_IP, json=data).text
        if SHUTTLE == 0:
            data = {'task': 'station_state', 'value': 'zero'}
        elif SHUTTLE == 1:
            data = {'task': 'station_state', 'value': 'one'}
        x = requests.post(IB_FLASK_IP, json=data).text
        # os.system("rosservice call /ur_hardware_interface/set_io 1 7 1")
        sleep(0.5)
        # os.system("rosservice call /ur_hardware_interface/set_io 1 7 0")

        raw_input("enter to start oil spreading")
        data = {'task': 'scissor_state', 'value': 'open'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(10)
        data = {'task': 'scissor_state', 'value': 'close'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(10)
        data = {'task': 'scissor_state', 'value': 'open'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(10)

        raw_input("enter to perform cuttng test")
        # cutting test
        print("cutting test")
        data = {'task': 'scissor_cuttest', 'value': 'testa'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(2)
        data = {'task': 'scissor_state', 'value': 'close'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(10)
        data = {'task': 'scissor_cuttest', 'value': 'visionstate'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(2)
        data = {'task': 'scissor_cuttest', 'value': 'materialroll'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(2)
        data = {'task': 'scissor_cuttest', 'value': 'drawback'}
        x = requests.post(IB_FLASK_IP, json=data).text
        sleep(2)

        raw_input("enter to inspect top, sides, btm and tip")
        # inspect top, sides, btm and tip
        print("inspecting top, sides, btm and tip of instrument")

        raw_input("enter to deactivate stand holder")
        # deactivate stand holder
        if SHUTTLE == 0:
            data = {'task': 'stand_zero', 'value': 'lay'}
        elif SHUTTLE == 1:
            data = {'task': 'stand_one', 'value': 'lay'}
        x = requests.post(IB_FLASK_IP, json=data).text

        raw_input("enter to inpsect hinge")
        # inspect hinge
        print("inspecting hinge")

        raw_input("enter to activate standholder and roatet out")
        # activate stand holder
        data = {'task': 'scissor_state', 'value': 'mid'}
        x = requests.post(IB_FLASK_IP, json=data).text
        if SHUTTLE == 0:
            data = {'task': 'stand_zero', 'value': 'stand'}
        elif SHUTTLE == 1:
            data = {'task': 'stand_one', 'value': 'stand'}
        x = requests.post(IB_FLASK_IP, json=data).text

        # rotate out and activate oil spray
        if SHUTTLE == 0:
            data = {'task': 'station_state', 'value': 'one'}
        elif SHUTTLE == 1:
            data = {'task': 'station_state', 'value': 'zero'}
        x = requests.post(IB_FLASK_IP, json=data).text
        # os.system("rosservice call /ur_hardware_interface/set_io 1 7 1")
        sleep(0.5)
        # os.system("rosservice call /ur_hardware_interface/set_io 1 7 0")
        x = requests.post(IB_FLASK_IP, json=data).text
        data = {'task': 'scissor_hold', 'value': 'release'}
        # ask chen hui to complete this part, worst case will stop here and he sent these manually
        return 'success'


class TransferToBrac(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])

    def execute(self, userdata):
        """trasnfer an inspected instrument back to empty bracket"""
        bracket_coor = get_pose()
        if bracket_coor:  # bracket coor will return None if ai or pcl server is problematic
            if pick_and_place("ib", SHUTTLE_COOR, "ib", bracket_coor):
                rospy.loginfo(
                    "succussfully transfer inspected instrument to bracket")
                return 'success'
            else:
                rospy.logerr("error during transferring")
                return 'fail'
        else:
            rospy.logerr(
                "bracket coor return none, something wrong with ai / pcl")
            return 'fail'


class TransferFromSB(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])

    def execute(self, userdata):
        u, v = get_coor(ai_vision_server.msg.AIServerGoal.UR5_TOOL)

        """trasnfer a spare instrument from spare bay to empty bracket"""
        bracket_coor = get_pose(u, v)
        if bracket_coor:
            if pick_and_place("sb", RING_9, "sb", bracket_coor):
                rospy.loginfo(
                    "succussfully transfer spare instrument to bracket")
                return 'success'
            else:
                rospy.logerr("error during transferring")
                return 'fail'
        else:
            rospy.logerr(
                "bracket coor return none, something wrong with ai / pcl")
            return 'fail'


class Debug(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])

    def execute(self, userdata):
        data = {'task': 'station_state', 'value': 'one'}
        x = requests.post(IB_FLASK_IP, json=data).text
        print(x)

        raw_input()
        data = {'task': 'station_state', 'value': 'zero'}
        x = requests.post(IB_FLASK_IP, json=data).text
        print(x)

        return 'success'
# SMACH CLASSES END ###########################################333


def main():
    try:
        sm = smach.StateMachine(outcomes=['ended'])
        with sm:
            smach.StateMachine.add('WaitForStart', WaitForStart(), transitions={
                                   'sb': 'TransferFromSB', 'ib': 'TransferToIB', 'fail': 'ended'})
            smach.StateMachine.add('TransferToIB', TransferToIB(), transitions={
                                   'success': 'ActivateIB', 'fail': 'ended'})
            smach.StateMachine.add('ActivateIB', ActivateIB(), transitions={
                                   'success': 'ended'})
            smach.StateMachine.add('TransferToBrac', TransferToBrac(), transitions={
                                   'success': 'ended', 'fail': 'ended'})

            smach.StateMachine.add('TransferFromSB', TransferFromSB(), transitions={
                                   'success': 'ended', 'fail': 'ended'})

            smach.StateMachine.add('Debug', Debug(), transitions={
                                   'success': 'ended'})
        smach_viewer = smach_ros.IntrospectionServer(
            'ur5_smach_controller', sm, '/Start')
        smach_viewer.start()
        outcome = sm.execute()
        rospy.spin()
        smach_viewer.stop()
    except rospy.ROSInterruptException:
        return 0
    except KeyboardInterrupt:
        return 0


if __name__ == "__main__":

    rospy.init_node('program_manager', anonymous=True)
    initialization()
    main()
