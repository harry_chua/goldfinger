#!/usr/bin/env python

import rospy
import smach
import smach_ros
import actionlib
import time
import os

import ai_vision_server.msg
import vision_handler.msg
import ur5_pap_actionlib.msg
from ur5_gripper.srv import *
import vision_handler.srv
import ib2_server.msg

import requests
from flask import json

from constants import *
from utils import *

ai_client = None
pcl_client = None
motion_client = None
ib2_client = None
all_zone = []  # vision_handler.srv.BracketZoneDetectorResponse()
origin_zone = []
target_zone = []
no_bracket_processed = 0
target_bracket = None
shuttle_full = False
first = True

SHUTTLE = 0
ZONE_A = (636, 791, 316, 480)  # For demo
ZONE_B = (634, 790, 525, 689)  # For demo
ZONE_C = (380, 535, 422, 586)  # For demo

### Connecting to all action servers and services ###


def initialization():
    global ai_client, pcl_client, motion_client, ib2_client, target_bracket

    rospy.loginfo("Starting program manager...")

    rospy.loginfo("Checking AI vision server...")
    ai_client = actionlib.SimpleActionClient(
        'ai_server', ai_vision_server.msg.AIServerAction)
    ai_client.wait_for_server()
    rospy.loginfo("AI vision server connected.")

    rospy.loginfo("Checking PCL server...")
    pcl_client = actionlib.SimpleActionClient(  # Name updating required
        'pcl_handling_server', vision_handler.msg.PCLHandlerAction)
    pcl_client.wait_for_server()
    rospy.loginfo("PCL server connected.")

    rospy.loginfo("Checking motion server...")
    motion_client = actionlib.SimpleActionClient(  # Name updating required
        'ur5_control_server', ur5_pap_actionlib.msg.MotionControlAction)
    motion_client.wait_for_server()
    rospy.loginfo("Motion server connected.")

    rospy.loginfo("Checking IB2 server...")
    ib2_client = actionlib.SimpleActionClient(
        'ib2_server', ib2_server.msg.IB2ServerAction)
    ib2_client.wait_for_server()
    rospy.loginfo("IB2 server connected.")

    rospy.loginfo("All servers connected!")

########## Execution functions ##########


def query_brackets():
    '''
    Check if brackets are ready to start the programme
    '''
    rospy.loginfo('Waiting brackets condition...')
    global target_bracket
    start = False
    target = None
    while(not start):
        ai_goal = ai_vision_server.msg.AIServerGoal()
        ai_goal.task = ai_vision_server.msg.AIServerGoal.CHECK_BRAC
        ai_client.send_goal(ai_goal)
        ai_client.wait_for_result()
        ai_result = ai_vision_server.msg.AIServerResult()
        ai_result = ai_client.get_result()

        if ai_result.detection_result:
            data = ai_result.detection_result
            if TEB in data:
                if MIB in data and target_bracket != MIB:
                    target = MIB
                    start = True
                elif FOB in data and target_bracket != FOB:
                    target = FOB
                    start = True
                else:
                    rospy.loginfo(
                        "No 'FOB' or 'MIB' found, continue detecting...")
                    time.sleep(5)
            else:
                rospy.logerr("Empty bracket not found!")
                return None
        else:
            rospy.logerr("No result found!")
            rospy.logerr("Retrying...")
            time.sleep(5)
    return target


def get_bracket_data(bracket):
    '''
    Get AI detected target details
    '''
    ai_goal = ai_vision_server.msg.AIServerGoal()
    ai_goal.task = ai_vision_server.msg.AIServerGoal.GET_BRAC
    ai_goal.target_des = bracket
    ai_client.send_goal(ai_goal)
    ai_client.wait_for_result()
    ai_result = ai_vision_server.msg.AIServerResult()
    ai_result = ai_client.get_result()
    return ai_result


def get_coor(tar_des, zone):
    '''
    Get coordinates of picking target within specific zone
    '''
    retry = 0
    while retry < 1000:
        ai_goal = ai_vision_server.msg.AIServerGoal()
        ai_goal.task = ai_vision_server.msg.AIServerGoal.PICK
        ai_goal.target_des = tar_des
        ai_client.send_goal(ai_goal)
        ai_client.wait_for_result()
        ai_result = ai_vision_server.msg.AIServerResult()
        ai_result = ai_client.get_result()
        if ai_result.success:
            # print(ai_result)  # For debugging
            if(len(zone)):
                filtered_data = filter_zone(ai_result.ai_result, zone)
                if filtered_data:
                    return filtered_data.u, filtered_data.v
                else:
                    retry += 1
                    time.sleep(3)
                    rospy.logerr("Retrying %d time..." % retry)
            else:
                return ai_result.ai_result[0].u, ai_result.ai_result[0].v
        else:
            rospy.logerr("Error during getting coordinates!")
            # Handle exception


def get_pose(u, v):
    '''
    Get location and depth of given point within camera frame
    '''
    return_pose = None
    pcl_goal = vision_handler.msg.PCLHandlerGoal()
    pcl_goal.task = pcl_goal.GET_INSTRUMENT_POSE
    pcl_goal.u = u
    pcl_goal.v = v
    pcl_client.send_goal(pcl_goal)
    pcl_client.wait_for_result()
    pcl_result = vision_handler.msg.PCLHandlerResult()
    pcl_result = pcl_client.get_result()
    if pcl_result.success:
        return_pose = pcl_result.targetPoseStamped.pose
        # print(return_pose.pose)
        return return_pose
    else:
        rospy.logerr("Error during getting pose!")
        return None


def pick(origin, pick_pose):
    '''
    Perform picking
    '''
    # Ensure 'Instrument Gripper' is selected
    motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
    motion_goal.task = motion_goal.CHANGE_EEF
    motion_goal.option = motion_goal.INSTRUMENT_GRIPPER
    motion_client.send_goal(motion_goal)
    motion_client.wait_for_result()

    if motion_client.get_result().success:
        # Move to respective fixed position
        motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
        if origin == "ib" or origin == "pt":
            motion_goal.task = motion_goal.MOVE_TO_PROCESSING_TABLE
        elif origin == "sb":
            motion_goal.task = motion_goal.MOVE_TO_SPARE_BAY
        motion_client.send_goal(motion_goal)
        motion_client.wait_for_result()

        if motion_client.get_result().desArrived:
            # Navigate to specified position and perform picking task
            motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
            motion_goal.task = motion_goal.PICK
            if origin == "sb":  # To be updated ###
                motion_goal.option = motion_goal.FROM_SPARE
            elif origin == 'ib':
                motion_goal.option = motion_goal.FROM_SHUTTLE
            motion_goal.goalPose = pick_pose
            motion_client.send_goal(motion_goal)
            motion_client.wait_for_result()

            if motion_client.get_result().success:
                # Move back to respective fixed position
                motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
                if origin == "ib" or origin == "pt":
                    motion_goal.task = motion_goal.MOVE_TO_PROCESSING_TABLE
                elif origin == "sb":
                    motion_goal.task = motion_goal.MOVE_TO_SPARE_BAY
                motion_client.send_goal(motion_goal)
                motion_client.wait_for_result()

                if motion_client.get_result().desArrived:
                    rospy.loginfo("Instrument picked successfully.")
                    return True
                else:
                    rospy.logerr("Error during travelling to fixed position!")
                    return False

            else:
                rospy.logerr("Error during picking!")
                return False

        else:
            rospy.logerr("Error during travelling to fixed position!")
            return False

    else:
        rospy.logerr("Error during changing gripper!")
        return False


def place(dest, place_pose):
    '''
    Perform placing
    '''
    # Move to respective fixed position
    motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
    if dest == "ib" or dest == "pt":
        motion_goal.task = motion_goal.MOVE_TO_PROCESSING_TABLE
    elif dest == "sb":
        motion_goal.task = motion_goal.MOVE_TO_SPARE_BAY
    motion_client.send_goal(motion_goal)
    motion_client.wait_for_result()

    if motion_client.get_result().desArrived:
        # Navigate to specified position and perform placing task
        motion_goal = ur5_pap_actionlib.msg.MotionControlGoal()
        if dest == "ib":
            motion_goal.task = motion_goal.PLACE_IN_SHUTTLE
            motion_goal.goalPose = list_to_pose(place_pose)
        elif dest == "pt":
            motion_goal.task = motion_goal.PLACE_IN_BRACKET
            motion_goal.goalPose = place_pose
        elif dest == "sb":
            motion_goal.task = motion_goal.PLACE_IN_BIN
            motion_goal.goalPose = list_to_pose(place_pose)
        motion_client.send_goal(motion_goal)
        motion_client.wait_for_result()

        if motion_client.get_result().success:
            rospy.loginfo("Instrument placed successfully.")
            return True
        else:
            rospy.logerr("Error during placing!")
            return False

    else:
        rospy.logerr("Error during travelling to fixed position!")
        return False


def query_ib2(action):
    '''
    Call inspection bay 2 action
    '''
    ib2_goal = ib2_server.msg.IB2ServerGoal()
    if action == IB2_Action.GET_STATUS:
        ib2_goal.task = ib2_server.msg.IB2ServerGoal.GET_STATUS
        ib2_client.send_goal(ib2_goal)
    elif action == IB2_Action.RESET:
        ib2_goal.task = ib2_server.msg.IB2ServerGoal.RESET
        ib2_goal.box_id = '000'  # To be changed
        ib2_client.send_goal(ib2_goal)
    elif action == IB2_Action.INSPECT:
        ib2_goal.task = ib2_server.msg.IB2ServerGoal.INSPECT
        ib2_client.send_goal(ib2_goal)
    elif action == IB2_Action.CALL_NEXT_TOOL:
        ib2_goal.task = ib2_server.msg.IB2ServerGoal.CALL_NEXT_TOOL
        ib2_client.send_goal(ib2_goal)

    ib2_client.wait_for_result()
    result = ib2_client.get_result()
    # rospy.loginfo(result)
    return result


def get_shuttle_pose():
    '''
    Get shuttle pose without offset
    '''
    rospy.wait_for_service('/aruco_detector/shuttle_coor')
    try:
        shuttle_coor_client = rospy.ServiceProxy(
            '/aruco_detector/shuttle_coor', vision_handler.srv.ArucoMarkerMidPt)
        shuttle_coor = shuttle_coor_client()
        # print(shuttle_coor)  # For debugging
        if (shuttle_coor):
            marker1 = shuttle_coor.marker1_coor
            marker2 = shuttle_coor.marker2_coor
            while (True):
                marker1_pose = get_pose(marker1[0], marker1[1])
                marker2_pose = get_pose(marker2[0], marker2[1])
                # Check if poses are calculated
                if marker1_pose.position.x and marker2_pose.position.x:
                    mid_x = (marker1_pose.position.x +
                             marker2_pose.position.x)/2
                    mid_y = (marker1_pose.position.y +
                             marker2_pose.position.y)/2
                    break
                else:
                    rospy.logerr(
                        "Shuttle pose return NONE, something wrong with PCL server!")
                    continue
            print('Shuttle midpoint:', mid_x, mid_y)  # For debugging purpose

            if mid_x and mid_y:
                return [mid_x, mid_y]
            else:
                rospy.logerr(
                    "Error during getting shuttle midpoints")
                return None

    except rospy.ServiceException as e:
        print("Shuttle coordinates querying service call failed: %s" % e)


########## SMACH CLASSES ##########


class WaitForStart(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['start',
                                             'process_table',
                                             'spare_bay',
                                             'inspect_bay',
                                             'locker',
                                             'fail'])

    def execute(self, userdata):
        print("Type ok to start or pt / sb / ib / lk for demo.")
        start = raw_input()
        if start == "ok":
            return 'start'
        elif start == "pt":
            return 'process_table'
        elif start == "pt":
            return 'process_table'
        elif start == "sb":
            return 'spare_bay'
        elif start == "ib":
            return 'inspect_bay'
        elif start == "lk":
            return 'locker'
        else:
            return 'fail'


class DefaultExecution(smach.State):
    def __init__(self):
        smach.State.__init__(
            self, outcomes=['ready', 'error'], output_keys=['lock_goal', 'stage_output'])

    def execute(self, userdata):
        global target_bracket, all_zone, origin_zone, target_zone
        rospy.wait_for_service('/aruco_detector/bracket_zone')
        try:
            brac_zone_client = rospy.ServiceProxy(
                '/aruco_detector/bracket_zone', vision_handler.srv.BracketZoneDetector)
            all_zone = [brac_zone_client().zoneA, brac_zone_client(
            ).zoneB, brac_zone_client().zoneC]
            print(all_zone)  # For debugging
        except rospy.ServiceException as e:
            print("Brackets zone querying service call failed: %s" % e)

        target = query_brackets()
        # Get TEB zone (placing zone)
        if target:
            ai_result = get_bracket_data(ai_vision_server.msg.AIServerGoal.TEB)

            if len(ai_result.ai_result):  # Assume only one TEB
                teb_data = ai_result.ai_result[0]
                target_zone = get_zone((teb_data.u, teb_data.v), all_zone)
                print(target_zone)  # For debugging
            else:
                rospy.logerr("TEB zone not found!")

            # Reset the inspection bay 2 (Shuttle 0 facing out)
            ib2_ready = False
            trial = 1
            rospy.loginfo("Resetting inpection bay 2...")
            while (not ib2_ready):
                ib2_res = query_ib2(IB2_Action.RESET)
                if ib2_res.status != 'AVAILABLE':
                    rospy.logerr("Inspection bay 2 is not ready yet!")
                    rospy.logerr("Retrying...%d" & trial)
                    trial += 1
                else:
                    ib2_ready = True

        # If condition reached, proceed to unlocking proces
        if target == FOB:
            ai_result = get_bracket_data(ai_vision_server.msg.AIServerGoal.FOB)

            if len(ai_result.ai_result):  # Assume only one FOB
                fob_data = ai_result.ai_result[0]
                origin_zone = get_zone((fob_data.u, fob_data.v), all_zone)
                # print(origin_zone)  # For debugging
            else:
                rospy.logerr("FOB zone not found!")

            # userdata.lock_goal = 'open'
            userdata.stage_output = 'next'
            target_bracket = FOB
            rospy.loginfo('Running FOB inspections')
            return 'ready'

        elif target == MIB:
            ai_result = get_bracket_data(ai_vision_server.msg.AIServerGoal.MIB)

            if len(ai_result.ai_result):  # Assume only one MIB
                mib_data = ai_result.ai_result[0]
                origin_zone = get_zone((mib_data.u, mib_data.v), all_zone)
                print(origin_zone)  # For debugging
            else:
                rospy.logerr("MIB zone not found!")

            # userdata.lock_goal = 'open'
            userdata.stage_output = 'next'
            target_bracket = MIB
            rospy.loginfo('Running MIB inspections')
            return 'ready'

        else:
            rospy.logerr(
                'Please reload a new empty bracket before starting the program!')
            return 'error'

# Stage: next, resume
### Checking mechanism to decide if FOB or MIB is completed or empty ###


class InspectionLoopControl(smach.State):
    def __init__(self):
        smach.State.__init__(
            self, outcomes=['pt_ib', 'post_inspection',
                            'continue', 'fail'],
            input_keys=['stage_input'],
            output_keys=['inspection_output', 'defective_tool', 'stage_output'])

    def execute(self, userdata):
        global target_bracket, all_zone, origin_zone, target_zone, shuttle_full
        if userdata.stage_input == 'next':
            # Check if origin zone is TEB:
            ai_result = get_bracket_data(ai_vision_server.msg.AIServerGoal.TEB)

            if len(ai_result.ai_result):  # Assume only one TEB
                teb_data = ai_result.ai_result[0]
                if(within_zone((teb_data.u, teb_data.v), origin_zone)):
                    return 'post_inspection'
                else:
                    userdata.stage_output = 'resume'
                    return 'continue'
            else:
                userdata.stage_output = 'resume'
                return 'continue'

        elif userdata.stage_input == 'resume':
            # Check inspection bay 2 availability
            ib2_res = query_ib2(IB2_Action.GET_STATUS)
            if ib2_res.status == 'INSPECTING':
                rospy.logerr("Inspection in progress!")
                # Check second shuttle status
                if not shuttle_full:
                    rospy.loginfo('Filling empty shuttle...')
                    shuttle_full = True
                    return 'pt_ib'
                else:
                    return 'post_inspection'
            elif ib2_res.status == 'AVAILABLE':
                if ib2_res.tool_processing == '':
                    return 'pt_ib'
                elif not shuttle_full:
                    rospy.loginfo('Filling empty shuttle...')
                    shuttle_full = True
                    return 'pt_ib'
                else:
                    return 'post_inspection'
            else:
                rospy.logerr("Error during getting inspection bay status!")
                return 'fail'


class HandleLocker(smach.State):
    def __init__(self):
        smach.State.__init__(
            self,  outcomes=['success', 'fail'], input_keys=['lock_goal'])

    def execute(self, userdata):
        # Locking and Unlocking
        rospy.loginfo("Hanlding Locker: {} for {} and 'TEB'" %
                      userdata.lock_goal, target_bracket)
        return 'success'

### Transfer instrument from bracket to inspection bay ###


class TransferFromPT(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'fail'])

    def execute(self, userdata):
        global SHUTTLE
        rospy.loginfo("\nExecuting Transfer: PT - IB")

        # Get gripping position
        u, v = get_coor(
            ai_vision_server.msg.AIServerGoal.UR5_TOOL, origin_zone)
        bracket_pose = get_pose(u, v)
        print(bracket_pose)  # For debugging purpose

        # Get placing position
        detected_shuttle_pose = get_shuttle_pose()
        shuttle_pose = SHUTTLE_PLACE_POSE
        shuttle_pose[0] = detected_shuttle_pose[0]
        shuttle_pose[1] = detected_shuttle_pose[1] - \
            0.0195  # To be tuned

        if bracket_pose and shuttle_pose:
            rospy.loginfo(
                "Activating scissor holder and deactivating solenoid clip")
            if SHUTTLE == 0:
                data = {'task': 'stand_zero', 'value': 'lay'}
                SHUTTLE = 1
            elif SHUTTLE == 1:
                data = {'task': 'stand_one', 'value': 'lay'}
                SHUTTLE = 0
            x = requests.post(IB_FLASK_IP, json=data).text
            data = {'task': 'scissor_hold', 'value': 'release'}
            x = requests.post(IB_FLASK_IP, json=data).text

            # if open_locker(bracket_coor):
            if pick("pt", bracket_pose):
                if place('ib', shuttle_pose):
                    rospy.loginfo(
                        "Succussfully transfer instrument to inpsection bay shuttle.")
                    return 'success'
                else:
                    rospy.logerr("Error during trasnferring!")
                    return 'fail'
            else:
                rospy.logerr("Error during trasnferring!")
                return 'fail'
            # else:
            # rospy.logerr("error during locker opening")
            # return 'fail'
        else:
            rospy.logerr(
                "Bracket pose return NONE, something wrong with AI / PCL server!")
            return 'fail'


class ActivateIB(smach.State):
    def __init__(self):
        smach.State.__init__(
            self, outcomes=['success', 'fail'], output_keys=['stage_output'])

    def execute(self, userdata):
        global first
        rospy.loginfo("\nActivating Inspection Process...")
        if first:  # Stupid, change this later!
            ib2_res = query_ib2(IB2_Action.INSPECT)
            print(ib2_res)
            first = False
        userdata.stage_output = 'next'
        return 'success'


class HandleInspectionResult(smach.State):
    def __init__(self):
        smach.State.__init__(
            self, outcomes=['success', 'completed', 'fail', 'start'], output_keys=['inspection_output', 'defective_tool'])

    def execute(self, userdata):
        global shuttle_full, first, no_bracket_processed
        rospy.loginfo("\nWaiting inspection process to be completed...")
        trial = 1
        while(True):
            ib2_res = query_ib2(IB2_Action.GET_STATUS)
            if ib2_res.status == 'INSPECTING':
                if ib2_res.tool_processing == '':
                    no_bracket_processed += 1
                    if no_bracket_processed != 2:
                        first = True
                        return 'start'
                    else:
                        os.sys('curl 192.168.10.196:5002/kill_process')
                    return 'completed'
                time.sleep(2)
                rospy.loginfo('Try retrieving result %d time...' % trial)
                trial += 1
                continue
            elif ib2_res.status == 'AVAILABLE':
                ib2_res = query_ib2(IB2_Action.CALL_NEXT_TOOL)
                if ib2_res.tool_status == 'GOOD':
                    userdata.inspection_output = 'good'
                    # Call inspect
                    query_ib2(IB2_Action.INSPECT)
                    shuttle_full = False
                    return 'success'
                elif ib2_res.tool_status == 'DEFECT':
                    userdata.inspection_output = 'defect'
                    userdata.defective_tool = ib2_res.defect_tool_name
                    # Call inspect
                    query_ib2(IB2_Action.INSPECT)
                    shuttle_full = False
                    return 'success'
                # elif ib2_res.tool_status == '' and no_bracket_processed != 2:
                #     first = True
                #     return 'start'
                else:
                    rospy.logerr('Error during getting inspection result!')
                    return 'fail'
            elif ib2_res.status == 'ERROR':  # To be changed
                no_bracket_processed += 1
                if no_bracket_processed != 2:
                    first = True
                    return 'start'
                else:
                    os.sys('curl 192.168.10.196:5002/kill_process')
                return 'completed'
            else:
                rospy.logerr('Error during getting inspection bay status!')
                return 'fail'


class TransferFromSB(smach.State):
    '''
    Transfer instrument from spare bay to empty bracket
    '''

    def __init__(self):
        smach.State.__init__(
            self,  outcomes=['success', 'fail'], input_keys=['tool_index'], output_keys=['stage_output'])

    def execute(self, userdata):
        rospy.loginfo("Executing Transfer: SB - PT")

        # Get placing position
        u, v = get_coor(
            ai_vision_server.msg.AIServerGoal.UR5_TOOL, target_zone)  # CHANGE THE ZONE A
        bracket_pose = get_pose(u, v)
        print(bracket_pose)  # For debugging purpose

        if bracket_pose and userdata.tool_index:
            if pick("sb", list_to_pose(userdata.tool_index)):
                if place('pt', bracket_pose):
                    rospy.loginfo(
                        "Succussfully transfer instrument to temporary bracket.")
                    userdata.stage_output = 'next'
                    return 'success'
                else:
                    rospy.logerr("Error during transferring!")
                    return 'fail'
            else:
                rospy.logerr("Error during transferring!")
                return 'fail'
        else:
            rospy.logerr(
                "Bracket pose return NONE, something wrong with AI / PCL server!")
            return 'fail'


class TransferFromIB(smach.State):
    def __init__(self):
        smach.State.__init__(
            self,  outcomes=['success', 'replace', 'fail'],
            input_keys=['inspection_input', 'defective_tool'], output_keys=['stage_output', 'ring_index'])

    def execute(self, userdata):
        rospy.loginfo("Executing Transfer: IB - PT")

        # Get picking position
        detected_shuttle_pose = get_shuttle_pose()
        shuttle_pose = SHUTTLE_PICK_POSE
        shuttle_pose[0] = detected_shuttle_pose[0]
        shuttle_pose[1] = detected_shuttle_pose[1] - \
            0.0165  # To be tuned

        # Defect free
        # if(True):
        if(userdata.inspection_input == 'good'):
            u, v = get_coor(
                ai_vision_server.msg.AIServerGoal.UR5_TOOL, target_zone)  # CHANGE THE ZONE A
            bracket_pose = get_pose(u, v)
            # print(bracket_pose)  # For debugging purpose

            if bracket_pose:
                print('Shuttle pose: ', shuttle_pose)  # For debugging purpose
                if pick('ib', list_to_pose(shuttle_pose)):  # To be updated
                    if place('pt', bracket_pose):
                        rospy.loginfo(
                            "Succussfully transfer instrument to temporary bracket.")
                        userdata.stage_output = 'next'
                        return 'success'
                    else:
                        rospy.logerr("Error during transferring!")
                        return 'fail'
                else:
                    rospy.logerr("Error during transferring!")
                    return 'fail'
            else:
                rospy.logerr(
                    "Bracket pose return NONE, something wrong with AI / PCL server!")
                return 'fail'
        # Defective
        else:
            for key, value in tool_types.items():
                if userdata.defective_tool == key:
                    tool_position = value
                    break

            if pick('ib', list_to_pose(shuttle_pose)):  # To be updated
                if place('sb', BIN_POSE):
                    rospy.loginfo(
                        "Succussfully transfer instrument to bin.")
                    userdata.ring_index = tool_position[0]
                    return 'replace'
                else:
                    rospy.logerr("Error during transferring!")
                    return 'fail'
            else:
                rospy.logerr("Error during transferring!")
                return 'fail'

########## SMACH CLASSES END ##########


def main():
    try:
        # Create SMACH state machine
        sm = smach.StateMachine(outcomes=['ended'])

        # Open the container
        with sm:
            # Container states
            # smach.StateMachine.add('WaitForStart', WaitForStart(), transitions={
            #                        'start': 'DefaultExecution',
            #                        'process_table': 'TransferFromPT',
            #                        'spare_bay': 'TransferFromSB',
            #                        'inspect_bay': 'TransferFromIB',
            #                        'locker': 'HandleLocker',
            #                        'fail': 'ended'})
            smach.StateMachine.add('DefaultExecution', DefaultExecution(),
                                   transitions={
                                       'ready': 'InspectionLoopControl', 'error': 'ended'},  # Locker
                                   remapping={
                                       'lock_goal': 'goal_state',
                                       'stage_output': 'stage'})  # To be removed

            smach.StateMachine.add('InspectionLoopControl', InspectionLoopControl(),
                                   transitions={
                                       'pt_ib': 'TransferFromPT',
                                       'post_inspection': 'HandleInspectionResult',
                                       'continue': 'InspectionLoopControl',
                                       'fail': 'ended'},
                                   remapping={
                                       'stage_input': 'stage',
                                       'stage_output': 'stage'})
            smach.StateMachine.add('TransferFromPT', TransferFromPT(), transitions={
                'success': 'ActivateIB',
                'fail': 'ended'
            })
            smach.StateMachine.add('ActivateIB', ActivateIB(),
                                   transitions={
                                       'success': 'InspectionLoopControl',
                                       'fail': 'ended'},
                                   remapping={'stage_output': 'stage'})
            smach.StateMachine.add('HandleInspectionResult', HandleInspectionResult(),
                                   transitions={
                                       'success': 'TransferFromIB',
                                       'completed': 'ended',  # InformMainServer
                                       'fail': 'ended',
                                       'start': 'DefaultExecution'},
                                   remapping={
                                       'inspection_output': 'inspection_result',
                                       'defective_tool': 'replacing_tool'})
            smach.StateMachine.add('TransferFromSB', TransferFromSB(),
                                   transitions={
                                       'success': 'InspectionLoopControl',
                                       'fail': 'ended'},
                                   remapping={'tool_index': 'ringSB_pose', 'stage_output': 'stage'})
            smach.StateMachine.add('TransferFromIB', TransferFromIB(),
                                   transitions={
                                       'success': 'InspectionLoopControl',
                                       'replace': 'TransferFromSB',
                                       'fail': 'ended'},
                                   remapping={
                                       'inspection_input': 'inspection_result',
                                       'defective_tool': 'replacing_tool',
                                       'stage_output': 'stage',
                                       'ring_index': 'ringSB_pose'})
            smach.StateMachine.add('HandleLocker', HandleLocker(),
                                   transitions={
                                       'success': 'InspectionLoopControl', 'fail': 'ended'},
                                   remapping={
                                       'lock_goal': 'goal_state'})

        # Create and start introspection server
        smach_viewer = smach_ros.IntrospectionServer(
            'ur5_smach_controller', sm, '/Start')
        smach_viewer.start()

        # Execute SMACH plan
        outcome = sm.execute()
        rospy.spin()
        smach_viewer.stop()

    except rospy.ROSInterruptException:
        return 0
    except KeyboardInterrupt:
        return 0


if __name__ == "__main__":
    rospy.init_node('program_manager', anonymous=True)
    initialization()
    main()
