#!/usr/bin/env python3

import time #

import pt2_visualization_utils as vis_util #
from object_detection.utils import label_map_util #
from object_detection.utils import ops as utils_ops #
from PIL import Image
import matplotlib.pyplot as plt
from io import StringIO
from collections import defaultdict
import glob #
import pathlib #
import cv2 #
import tensorflow as tf #
import sys #
import numpy as np
import os #

import actionlib #
from cv_bridge import CvBridge #
import rospy #
import rospkg
import sensor_msgs.msg #
import ai_vision_server.msg

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #
rp = rospkg.RosPack()
tf.get_logger().setLevel('ERROR') #

PATH_TO_CONFIG = rp.get_path('ai_vision_server') + '/config/model_pt2_v4/'

# AI Model Loader
def load_ai_model():
    # Enable GPU dynamic memory allocation
    gpus = tf.config.experimental.list_physical_devices('GPU')
    print(gpus)
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)
    # example -> workspace/models/my_maskrcnn/micro_300/inference_graph
    model_path = PATH_TO_CONFIG + "saved_model"
    print('Loading model...')
    start_time = time.time()
    model = tf.saved_model.load(str(model_path))
    elapsed_time = time.time() - start_time
    print('Done! Took {} seconds.'.format(elapsed_time))
    
    labels_path = PATH_TO_CONFIG + "label_map.pbtxt"
    category_index = label_map_util.create_category_index_from_labelmap(
        labels_path, use_display_name=True)
    return model, category_index

# List of the strings that is used to add correct label for each box.
# example -> 'workspace/annotations/Micro/label.pbtxt'
# PATH_TO_LABELS = os.path.join('./', 'label_map.pbtxt')
# category_index = label_map_util.create_category_index_from_labelmap(
#    PATH_TO_LABELS, use_display_name=True)

##### For Debugging Purpose #####
# Path for test images
# example -> microcontroller-segmentation/test
# PATH_TO_TEST_IMAGES_DIR = pathlib.Path('ai_images/test_dataset/')
# TEST_IMAGE_PATHS = glob.glob(os.path.join(
#     PATH_TO_TEST_IMAGES_DIR, "frame0019.jpg"))
# print(TEST_IMAGE_PATHS)

# patch tf1 into `utils.ops`
# utils_ops.tf = tf.compat.v1

# Patch the location of gfile
# tf.gfile = tf.io.gfile

class AIServer:
    def __init__(self, model, category_index):
        self.model = model
        self.cat_index = category_index

        self.server = actionlib.SimpleActionServer(
            'ai_server', ai_vision_server.msg.AIServerAction, execute_cb=self.execute, auto_start=False)
        self.server.start()
        self.bridge = CvBridge()

    def execute(self, goal):
        print("\nAI vision service called!")
        start_time = time.time()

        ros_img_data = rospy.wait_for_message(
            '/stereo/left/image_rect_color', sensor_msgs.msg.Image)
        cv_img_data = self.bridge.imgmsg_to_cv2(
            ros_img_data, "bgr8")  # To be confirmed
        
        image_np = np.array(cv_img_data)

        # When writing a TensorFlow program, the main object that is manipulated and passed around is the tf.Tensor.
        # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
        input_tensor = tf.convert_to_tensor(image_np)

        # The model expects a batch of images, so add an axis with `tf.newaxis`.
        input_tensor = input_tensor[tf.newaxis, ...]

        # Perform inference
        model_fn = self.model.signatures["serving_default"]
        result_dict = model_fn(input_tensor)

        # All outputs are batches tensors.
        # Convert to numpy arrays, and take index [0] to remove the batch dimension.
        # We're only interested in the first num_detections.

        num_detections = int(result_dict.pop('num_detections'))
        need_detection_key = [
            'detection_classes', 'detection_boxes', 'detection_masks', 'detection_scores']
        result_dict = {key: result_dict[key][0, :num_detections].numpy(
        ) for key in need_detection_key}
        
        result_dict['num_detections'] = num_detections

        # Detection classes should be ints
        result_dict['detection_classes'] = result_dict['detection_classes'].astype(
            np.int64)

        # Handle models with masks
        if 'detection_masks' in result_dict:
            # Reframe the bbox mask to the image size
            detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(tf.convert_to_tensor(
                result_dict['detection_masks']), result_dict['detection_boxes'], image_np.shape[0], image_np.shape[1])
            detection_masks_reframed = tf.cast(
                detection_masks_reframed > 0.5, tf.uint8)
            result_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
        
        # print(result_dict.get('detection_masks_reframed', None)) # For debugging

        # Visualization of the results of a detection
        output = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            result_dict['detection_boxes'],
            result_dict['detection_classes'],
            result_dict['detection_scores'],
            self.cat_index,
            instance_masks=result_dict.get('detection_masks_reframed', None),
            use_normalized_coordinates=True,
            line_thickness=8
        )
        print(output)

        ##### Check goal requirements and generate actionlib response #####
        result = ai_vision_server.msg.AIServerResult()

        # target_outputs = []
        if goal.task == goal.PICK:
            target_outputs = []
            if goal.target_des == goal.UR5_TOOL:
                for x in output:
                    if x[0] == "RGP":
                        target_outputs.append(x)
                        # break
            else:
                for x in output:
                    if x[0] == "CGP":
                        target_outputs.append(x)
                        # break
            
            if len(target_outputs):
                for x in target_outputs:
                    try:
                        result_obj = ai_vision_server.msg.AI_output()
                        result_obj.item = x[0]
                        result_obj.u = x[1]
                        result_obj.v = x[2]
                        result_obj.theta = x[3]
                        result_obj.height = x[4]
                        result_obj.width = x[5]
                        result_obj.probability = x[6]
                        result.ai_result.append(result_obj)
                        result.success = True
                    except:
                        result.success = False
            
            else:
                rospy.logwarn("No result found!")
                result.success = False

        elif goal.task == goal.HANDLE_LOCK:
            print("To be developed...")

        elif goal.task == goal.CHECK_BRAC:
            detected_classes = []
            for x in output:
                detected_classes.append(x[0])
            result.detection_result = detected_classes
        
        elif goal.task == goal.GET_BRAC:
            target_outputs = []
            if goal.target_des == goal.TEB:
                for x in output:
                    if x[0] == "TEB":
                        target_outputs.append(x)
            elif goal.target_des == goal.FOB:
                for x in output:
                    if x[0] == "FOB":
                        target_outputs.append(x)
            elif goal.target_des == goal.MIB:
                for x in output:
                    if x[0] == "MIB":
                        target_outputs.append(x)

            if len(target_outputs):
                for i in range(len(target_outputs)):
                    try:
                        result_obj = ai_vision_server.msg.AI_output()
                        result_obj.item = target_outputs[i][0]
                        result_obj.u = target_outputs[i][1]
                        result_obj.v = target_outputs[i][2]
                        result_obj.theta = target_outputs[i][3]
                        result_obj.height = target_outputs[i][4]
                        result_obj.width = target_outputs[i][5]
                        result_obj.probability = target_outputs[i][6]
                        result.ai_result.append(result_obj)
                        result.success = True
                    except:
                        result.success = False
            
            else:
                rospy.logwarn("No result found!")
                result.success = False
            

        end_time = time.time()
        # try:
        #     cv2.circle(image_np, (result.ai_result.u))
        # except:
        #     pass
        
        # cv2.imshow("Output", cv2.resize(image_np, (800, 600)))
        cv2.imwrite(rp.get_path('ai_vision_server') +
                    '/config/ai_images/results/ai_vision_server.jpg', image_np)
        print(result)

        print('Detection done, took: ' + str(end_time-start_time))
        self.server.set_succeeded(result)

if __name__ == "__main__":
    rospy.init_node("ai_server", anonymous=True)
    detection_model, cat_index = load_ai_model()
    server = AIServer(detection_model, cat_index)
    rospy.loginfo("AI Server READY!")
    rospy.spin()
